#include "Gtk_Spectral.h"
#include <libintl.h>
#define _(String) gettext (String)
Gtk_Spectral::Gtk_Spectral(): m_btnReset(Gtk::Stock::FULLSCREEN) //The icon of reset button
{
     
     m_cboNoteChoosen=Gtk::manage(new Gtk::ComboBox);  //Get the pointer of the combo
     m_ItemCombo.set_expand(false); 
     if (m_cboNoteChoosen)
     {    
	  m_cboNoteChoosen->set_tooltip_text(_("Zooming to the note"));
	  // Connection to the function cbochanged
	  m_cboNoteChoosen->signal_changed().connect(sigc::mem_fun(*this,&Gtk_Spectral::on_cboNoteChoosen_changed));
	  m_refTreeModel_NoteChoosen=Gtk::ListStore::create(m_Columns_NoteChoosen);
	  m_cboNoteChoosen->set_model(m_refTreeModel_NoteChoosen);
	  m_ItemCombo.add(*m_cboNoteChoosen); //Add the cbo to the ItemCombo
     }
     m_btnReset.set_tooltip_text(_("Reset Zooming")); //Set the tooltiptext to the reset button
     //Connection to the function btnReset
     m_btnReset.signal_clicked().connect(sigc::mem_fun(*this,&Gtk_Spectral::on_btnResetZoom_clicked));   
     //Add the button and combo to the toolbar
     m_toolbar.append(m_ItemCombo);
     m_toolbar.append(tool_separator);
     m_toolbar.append(m_btnReset);
     m_toolbar.show_all();
     vbox.pack_start(m_toolbar,Gtk::PACK_SHRINK);
     vbox.add(m_FFTDisplay);
     add(vbox);
     show_all_children();
}
//The destructor
Gtk_Spectral::~Gtk_Spectral()
{
     delete m_cboNoteChoosen;
}
//Return a Gtk_DrawingSpectral pointer
Gtk_DrawingSpectral* Gtk_Spectral::GetDrawingAera(void)
{
     return &m_FFTDisplay;
}
//Set the parameters 
void Gtk_Spectral::SetParameters(CChord Chord)
{
     m_Chord=Chord;
     int j=0;
     Gtk::TreeModel::Row row;
     if (m_cboNoteChoosen)
     {
	  //If few notes in the chord
	  if (m_Chord.GetNumberNote()>1)
	  {
	       //The first row is the name Chord
	       row=*(m_refTreeModel_NoteChoosen->append());
	       row[m_Columns_NoteChoosen.m_col_id]=0;
	       row[m_Columns_NoteChoosen.m_col_name]=_("Chord");
	       j=1;	
	  }
	  //And the others, the note name
	  for (int i=0; i<m_Chord.GetNumberNote();i++)
	  {
	       row=*(m_refTreeModel_NoteChoosen->append());
	       row[m_Columns_NoteChoosen.m_col_id]=j+i;
	       row[m_Columns_NoteChoosen.m_col_name]=m_Chord.GetNote(i)->GetNoteName();
	  }
	  m_cboNoteChoosen->pack_start(m_Columns_NoteChoosen.m_col_name);
	  m_cboNoteChoosen->set_active(0);
     }
}
//The function when the combo change
void Gtk_Spectral::on_cboNoteChoosen_changed()
{
     CChord Chord;
     int IndexNoteChoosen;
     IndexNoteChoosen=m_cboNoteChoosen->get_active_row_number();
     Chord=m_Chord;
     //If it's not the first note
     if (IndexNoteChoosen!=0)
     {
	  //Clear all the notes
	  Chord.ClearNotes();
	  Chord.AddNote(*m_Chord.GetNote(IndexNoteChoosen-1));
	  Chord.SetChord(m_Chord.GetNote(IndexNoteChoosen-1)->GetNoteName());  //Put the choosen note in the new chord
     }
     m_FFTDisplay.SetChord(Chord); //Change the Gtk_DrawingSpectral with this note
}
//Reset the initial zoom of the  Gtk_DrawingSpectral
void Gtk_Spectral::on_btnResetZoom_clicked()
{
     m_FFTDisplay.UpdateMinMaxFrequency();
}
