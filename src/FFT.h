// FFT.h: interface for the CFFT class.
//
//////////////////////////////////////////////////////////////////////
#if !defined(AFX_FFT_H__D151C22F_A3E0_4036_8CA0_38B13166A38B__INCLUDED_)
#define AFX_FFT_H__D151C22F_A3E0_4036_8CA0_38B13166A38B__INCLUDED_
#include <math.h>
#include <iostream>

class CFFT
{
public:
	//Calculate the FFT with the samples in RealIn. The result is in RealOut and Imagout
	void fft_float(unsigned NumSamples,int InverseTransform, double *RealIn,double *ImagIn,double *RealOut, double *ImagOut );
	CFFT();
	virtual ~CFFT();
private:
	unsigned ReverseBits(unsigned index,unsigned NumBits);	//Inverse the data
	unsigned NumberOfBitsNeeded(unsigned PowerOfTwo);	// Get the power of two
	static void CheckPointer(void *p,std::string name);	//Check the validity of the pointer
	int IsPowerOfTwo(unsigned x);	//Verify if x is power of two
};
#endif // !defined(AFX_FFT_H__D151C22F_A3E0_4036_8CA0_38B13166A38B__INCLUDED_)
