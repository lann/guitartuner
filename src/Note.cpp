#include "Note.h"
#include <sstream>
#include <math.h>
#include <libintl.h>
#define _(String) gettext (String)

const double CNote::m_fNoteStandard[17]=
{
	//The notes frequencies
	32.703203125,	//C0 etc...
	34.6472125,//C#0
	34.6472125,//Db0
	36.708125,//D0
	38.890859375,//D#0
	38.890859375,//Eb0
	41.2034375,//E0
	43.653515625,//F0
	46.249296875,//F#0
	46.249296875,//Gb0
	48.9994523125,//G
	51.913125,//G#0
	51.913125,//Ab0
	55,//A0
	58.27046875,//A#0
	58.27046875,//Bb0
	61.735390625,//B0
};

//default constructor
CNote::CNote()
{
	Initialisation();	//variable initialisation
	m_bvalid=false;  //validity equal false
}

//constructor with the name of the note
CNote::CNote(std::string NoteName)
{
     m_szNoteName=NoteName; //set the name of the note
     Initialisation(); //variable initialisation
     CalculateNote(); //Calculate the frequency of the note
}

CNote::CNote(double Frequency)
{
     m_FreqNote=Frequency;
     Calculate_String_Note_Name();
}

//destructor
CNote::~CNote()
{
}

void CNote::Initialisation()
{
     m_FreqNote=0; //the frequency by default is equal to zero
}

std::string CNote::GetNoteName(void)
{
     return m_szNoteName; //return the Note Name value
}

void CNote::SetNoteName(std::string NoteName)
{
     m_szNoteName=NoteName; //set the note name
     CalculateNote();  //Calculate the frequency of the note
}
double CNote::GetFrequency(void)
{
     return m_FreqNote; //return the frequency of the note
}

void CNote::Calculate_String_Note_Name(void)
{
     int Harmonic=0;
     size_t j=0;
     std::ostringstream numeric;
     bool changed_harmonic(false);
//     Search the Harmonic of the Note
     while(m_FreqNote>(double)m_fNoteStandard[16]*pow(2,Harmonic))
     {
	  Harmonic++;
     }
     while((m_FreqNote>(double)m_fNoteStandard[j]*pow(2,Harmonic)) && (j<17))
     {
	  j++;
     }
     m_Freq_above=(double)m_fNoteStandard[j]*pow(2,Harmonic);
     if (j==0)
     {
	  m_Freq_below=(double)m_fNoteStandard[16]*pow(2,Harmonic-1);
	  changed_harmonic=true;
     }
     else
     {
	  m_Freq_below=(double)m_fNoteStandard[j-1]*pow(2,Harmonic);
     }
     if (m_FreqNote>(m_Freq_above-m_Freq_below)/2 + m_Freq_below)
     {
	  numeric<<Harmonic;
	  m_szNoteName=Name_Note[j]+numeric.str();
     }
     else if ((m_FreqNote<(m_Freq_above-m_Freq_below)/2 + m_Freq_below) && (!changed_harmonic))
     {
	  numeric<<Harmonic;
	  m_szNoteName=Name_Note[j-1]+numeric.str();
     }
     else if ((m_FreqNote<(m_Freq_above-m_Freq_below)/2 + m_Freq_below) && (changed_harmonic))
     {
	  numeric<<Harmonic-1;
	  m_szNoteName=Name_Note[16]+numeric.str();
     }
}

void CNote::CalculateNote(void)
{
     int Harmonic, comparison=1;
     int j=0;
     std::string Note, Level;
     //For all the char of the note
     for (int i=0;i<m_szNoteName.size();i++)
     {
	  std::string Temp;
	  int number;
	  Temp=m_szNoteName.at(i);
	  std::istringstream numeric(Temp);
	  numeric>>number;
	  //if the char is numeric then save
	  if (!numeric.fail())
	       Level+=Temp;
     }
     
     //Transform the number string in a int
     std::istringstream iss(Level);
     iss>>Harmonic;
     //Get the letter of the note (A, B ...Etc)
     Note.append(m_szNoteName,0,m_szNoteName.size()-Level.size());
     //Search the index of the note
     while((comparison!=0) && (j<17))
     {
	  comparison=Name_Note[j].compare(Note);
	  j++;
     }
     //If note found then the validity=false
     if (comparison!=0)
     {
	  m_bvalid=false;
     }
     else  //else the validity=true and the frequency of the note is calculated
     {
	  m_FreqNote=(double)m_fNoteStandard[j-1]*pow(2,Harmonic);
	  m_bvalid=true;
     }
}

bool CNote::GetValidity(void)
{
     return m_bvalid; //return the validty of the note
}

void CNote::SetFrequency(double Frequency)
{
     m_FreqNote=Frequency; //set the frequency of the note
     Calculate_String_Note_Name();
     m_bvalid=true; //the note is valid
}

double CNote::GetFrequencyAbove()
{
     return m_Freq_above;
}

double CNote::GetFrequencyBelow()
{
     return m_Freq_below;
}
