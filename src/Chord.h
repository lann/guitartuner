// Chord.h: interface for the CChord class.
//
//////////////////////////////////////////////////////////////////////
#if !defined(AFX_CHORD_H__71CB57B9_51D3_4A4A_8B02_801FEA969C6C__INCLUDED_)
#define AFX_CHORD_H__71CB57B9_51D3_4A4A_8B02_801FEA969C6C__INCLUDED_
#include <iostream>
#include <vector>
#include "Note.h"

class CChord
{
public:
     int GetNoteAbo(double Freq);    //Return the index of above note
     int GetNoteBel(double Freq);   //Return the index of below note
     std::string GetNameChord(void);	//Return the name of the chord
     void SetChord(std::string NameChord);	//Update the name of the chord
     bool GetValidity();  //return the validity of the note
     CNote* GetNote(int Index);  //return a pointer of the note 
     void AddNote(CNote Note);  //Add a Note to the chord
     void DelNote(int Index);  //Delete a Note to the chord
     void ClearNotes(void);  //Delete all the notes of the chord
     double GetNoteValue(int Index);	//Return the frequency of the note
     int GetNumberNote(void);  //return the number of the note
     CChord(std::string NameChord);	//create the class with the chord name
     CChord();	
     virtual ~CChord();      
private:	
     std::vector<CNote> m_Notes;	//Notes vector
     std::string m_szNameChord;	//Name of the chord
     bool m_bvalid;  //the validity of the chord
};
#endif // !defined(AFX_CHORD_H__71CB57B9_51D3_4A4A_8B02_801FEA969C6C__INCLUDED_)
