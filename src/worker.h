#ifndef WORKER_H
#define WORKER_H
#include <gtkmm.h>
#include <thread>
#include <mutex>
#include "FFT.h"
#include "Variables.h"

class CSoundCard;

class Worker
{
public:
     Worker();
     ~Worker();
     void do_work(CSoundCard *caller);
     void stop_work();
private:
     mutable std::mutex m_Mutex;
     bool m_shall_stop;
     s_data *data;
     CFFT fft;
};
#endif //WORKER
