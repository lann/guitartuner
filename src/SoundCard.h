//SoundCard.h : interface for the CSoundCard class
// CSoundCard represent a class to get the property of the sound card :
// - parameters
// * buffer
#ifndef SOUNDCARD_H_INCLUDED
#define SOUNDCARD_H_INCLUDED
#include <portaudio.h>
#include <vector>
#include <string>
#include "Variables.h"
#include "worker.h"
#include "Gtk_Spectral.h"
#include <gtkmm/dialog.h>
#define FRAMES_PER_BUFFER (512)
class CSoundCard
{
public :
     CSoundCard();
     ~CSoundCard();
     std::vector<std::string> GetListSndCard(void); //return a copy of vector with the list of the sound cards names
     std::vector<unsigned int> GetListIndexSndCard(void); //return the indexes of the hardware devices
     std::vector<unsigned int> GetListRateSndCard(unsigned int index); //return the rates list of a Sound Card
     std::string GetNameSndCard(PaDeviceIndex index_device); //return the name of the device
     void  ScanListSndCard(void);     //Scan all sound devices and record her names
     int SetParameters(parameters &param);  //Set the parameters for the sound card
     void StopRecord();  //Stop record samples
     void StartRecord(parameters param, CChord Notes_Chord, bool calibration);  //Start record samples
     bool IsRecordStarted(void); //Return true if the stream is started
     bool IsThreadStarted(void); //Return true if the thread is started
     void notify(); //Notify when the a FFT calculation is finished
     s_data* Get_Data(); //Return the struct data
     out* Get_Data_Out(); //Return the struct_data_out for analyse the FFT
     bool Is_Data_Freshed(void); //Return if new data output is available
     void Calibrate(parameters param, CChord Notes_Chord); //Sound Card Calibration
     double * Get_Calibration(void);  //Return the data of calibration
     double Get_Level(void); //Return the level of the sound card
private:
     static int recordCallback( const void *inputBuffer, void *outputBuffer, unsigned long framesPerBuffer, const PaStreamCallbackTimeInfo* timeInfo, PaStreamCallbackFlags statusFlags, void *userData );
     void ShowError(PaError error); //Show the Portaudio error
     void on_notification_from_worker_thread(void); //Call when the FFT is finished to calculate
     void SupPointers_data(void);
     void Initialisation_Sound_Card(const PaDeviceInfo *deviceinfo);
     std::vector<std::string> v_namesndcards; //The list of the sound card devices
     std::vector<std::vector <unsigned int>> v_ratesndCards; //The rates list of the sound cards
     std::vector<unsigned int> v_indexsndCards; //The index listed of the sound card
     PaStreamParameters inputParameters; //Parameters for the stream;
     Gtk_Spectral *m_wndSpectral;  //Spectral Window
     Gtk::Dialog dlg_calibrate;
     Gtk::ProgressBar pgb_calibrate;
     s_data data;
     double *m_calibration; //data for calibration
     PaStream *stream;
     Worker m_Worker;
     std::thread *m_WorkerThread;
     Glib::Dispatcher m_Dispatcher;
     bool fresh_data;
     bool bSpectralDisplayed; //Spectral Displayed ?
     parameters m_param_xml;
     size_t m_time_calibration;
     double m_level;
};
#endif //CARTESONS_H_INCLUDED
