#include <gtkmm.h>
#include "SoundCard.h"
#include <libintl.h>
#define _(String) gettext (String)

CSoundCard::CSoundCard() : m_WorkerThread(nullptr), m_Worker(), m_Dispatcher(), fresh_data(false), bSpectralDisplayed(false), dlg_calibrate(_("Calibrate"))
{
     ScanListSndCard();
     stream=nullptr;
     data.RealIn=nullptr;
     data.data_out.RealOut=nullptr;
     data.ImagIn=nullptr;
     data.data_out.ImagOut=nullptr;
     m_Dispatcher.connect(sigc::mem_fun(*this, &CSoundCard::on_notification_from_worker_thread));
     m_wndSpectral=NULL;
     m_calibration=nullptr;
     dlg_calibrate.get_vbox()->pack_start(pgb_calibrate);
     dlg_calibrate.set_resizable(false);
     dlg_calibrate.show_all_children();
}

CSoundCard::~CSoundCard()
{
     SupPointers_data();
     if (m_WorkerThread!=nullptr)
     {
	  delete m_WorkerThread;
	  m_WorkerThread=nullptr;
     }
     if (m_calibration)
     {
	  delete m_calibration;
	  m_calibration=nullptr;
     }
}

void CSoundCard::SupPointers_data(void)
{
     if (data.ImagIn!=nullptr)
     {
	  delete data.ImagIn;
	  data.ImagIn=nullptr;
     }
     if (data.data_out.ImagOut!=nullptr)
     {
	  delete data.data_out.ImagOut;
	  data.data_out.ImagOut=nullptr;
     }
     if (data.RealIn!=nullptr)
     {
	  delete data.RealIn;
	  data.RealIn=nullptr;
     }
     if (data.data_out.RealOut!=nullptr)
     {
	  delete data.data_out.RealOut;
	  data.data_out.RealOut=nullptr;
     }
     if (m_wndSpectral)
     {
	  delete m_wndSpectral;
	  m_wndSpectral=NULL;
     }
}

std::vector<std::string> CSoundCard::GetListSndCard(void)
{
     return v_namesndcards;
}

std::vector<unsigned int> CSoundCard::GetListIndexSndCard(void)
{
     return v_indexsndCards;
}

std::vector<unsigned int> CSoundCard::GetListRateSndCard(unsigned int index)
{
     return v_ratesndCards.at(index);
}

double * CSoundCard::Get_Calibration(void)
{
     return m_calibration;
}

void CSoundCard::StartRecord(parameters param, CChord Notes_Chord, bool calibration)
{
     //Prepare the sound card
     PaError error;

     data.num_Samples=param.nNumberSample;
     data.period=param.period;
     data.m_calibration=calibration;
     if (data.RealIn==nullptr)
     {
	  data.RealIn=new double[data.num_Samples];
     }
     if (data.ImagIn==nullptr)
     {
	  data.ImagIn=new double[data.num_Samples];
     }
     if ((data.RealIn!=nullptr) && (data.ImagIn!=nullptr))
     {
	  for(size_t i=0;i<data.num_Samples;i++)
	  {
	       data.RealIn[i]=0;
	       data.ImagIn[i]=0;
	  }
     }
     error=Pa_OpenStream(&stream, &inputParameters, NULL, param.nFreqSampling, FRAMES_PER_BUFFER, paClipOff, recordCallback, &data);
     if (error==paNoError)
     {
	  error=Pa_StartStream(stream);
	  if (error==paNoError)
	  {
	       if (m_WorkerThread==nullptr)
	       {
		    if (data.data_out.RealOut==nullptr)
		    {
			 data.data_out.RealOut=new double[data.num_Samples];
		    }
		    if (data.data_out.ImagOut==nullptr)
		    {
			 data.data_out.ImagOut=new double[data.num_Samples];
		    }
		    for (size_t i=0; i<data.num_Samples;i++)
		    {
			 data.data_out.ImagOut[i]=0;
			 data.data_out.RealOut[i]=0;
		    }
		    if (param.bDispFreq && !calibration)
		    {
			 if ((data.data_out.ImagOut!=nullptr) && (data.data_out.RealOut!=nullptr))
			 {
			      if (!bSpectralDisplayed)
			      {
				   //If Spectral window is enabled
				   m_wndSpectral=new Gtk_Spectral;
				   if (m_wndSpectral)
				   {
					m_wndSpectral->SetParameters(Notes_Chord);
					if (!m_wndSpectral->GetDrawingAera()->Get_Parameters_Validity())
					{
					     m_wndSpectral->GetDrawingAera()->SetParameters(
						  Notes_Chord, param, data.data_out.ImagOut,
						  data.data_out.RealOut, m_calibration);
					}

					// Maximize the Spectral window
					m_wndSpectral->maximize();
					// Start the window
					m_wndSpectral->show();
					m_wndSpectral->show_all_children();
					bSpectralDisplayed=true;
				   }
			      }
			 }
		    }
		    fresh_data=false;
		    m_WorkerThread=new std::thread([this]
							{
							     m_Worker.do_work(this);
							});
	       }
	       while((error=Pa_IsStreamStopped(stream))==0)
	       {
		    if (Gtk::Main::events_pending())
		    {
			 Gtk::Main::iteration();
		    }
	       }
	       if ((stream!=0) && (error<0))
	       {
		    ShowError(error);
	       }
	  }
	  else
	  {
	       ShowError(error);
	  }
     }
     else
     {
	  ShowError(error);
     }
}

void CSoundCard::Calibrate(parameters param, CChord Notes_Chord)
{
     if (m_calibration==nullptr)
     {
	  m_calibration=new double[param.nNumberSample];
     }
     m_time_calibration=0;
     m_level=0;
     pgb_calibrate.set_fraction(0.0);
     dlg_calibrate.show();
     StartRecord(param, Notes_Chord, true);
}

int CSoundCard::recordCallback(const void *inputBuffer, void *outputBuffer, unsigned long framesPerBuffer, const PaStreamCallbackTimeInfo* timeInfo, PaStreamCallbackFlags statusFlags, void *userData )
{
     s_data *data=(s_data*)userData;
     unsigned long SamplePlayed; //Quantity of samples played
     const float *rptr=(const float*)inputBuffer;
     (void) statusFlags;
     (void) timeInfo;
     (void) outputBuffer;
     SamplePlayed=framesPerBuffer/data->period;
     for (size_t i=framesPerBuffer;i<data->num_Samples;i++)
     {
	  data->RealIn[i-framesPerBuffer]=data->RealIn[i];
     }
     for (size_t i=data->num_Samples-framesPerBuffer;i<data->num_Samples;i++)
     {
	  data->RealIn[i]=(double)*rptr++;
     }
     return paContinue;
}

bool CSoundCard::IsRecordStarted(void)
{
     return Pa_IsStreamActive(stream);
}

bool CSoundCard::IsThreadStarted()
{
     if (m_WorkerThread)
     {
	  return m_WorkerThread->joinable();
     }
     else
     {
	  return false;
     }
}

void CSoundCard::ShowError(PaError error)
{
     std::ostringstream ostr;
     std::string message(_("Problem with Portaudio\nThe error code is : "));
     ostr<<error;
     message+=ostr.str();
     message+=(_("\nThe signification of this error : "));
     message+=Pa_GetErrorText(error);
     Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
     Dialog.set_title(_("Sound card problem"));
     Dialog.run();
}

void CSoundCard::on_notification_from_worker_thread(void)
{
     fresh_data=true;
     if ((m_param_xml.bDispFreq) && (data.data_out.RealOut!=nullptr) && (data.data_out.ImagOut!=nullptr) && (!data.m_calibration))
     {
	  //And the spectral window if it's selected
	  Glib::RefPtr<Gdk::Window> win = m_wndSpectral->GetDrawingAera()->get_window();
	  if (win)
	  {
	       Gdk::Rectangle r(0, 0,  m_wndSpectral->GetDrawingAera()->get_allocation().get_width(),
				m_wndSpectral->GetDrawingAera()->get_allocation().get_height());
	       win->invalidate_rect(r, false);
	  }
     }
     if (data.m_calibration && m_time_calibration<=m_param_xml.nCalibratePeriod)
     {
	  for (size_t i=0; i<data.num_Samples;i++)
	  {
	       if (m_time_calibration==0)
	       {
		    m_calibration[i]=sqrt(pow(data.data_out.RealOut[i],2)+pow(data.data_out.ImagOut[i],2));
	       }
	       else
	       {
		    m_calibration[i]=(m_calibration[i]*m_time_calibration+sqrt(pow(data.data_out.RealOut[i],2)+pow(data.data_out.ImagOut[i],2)))/(m_time_calibration+1);
	       }
	  }
	  pgb_calibrate.set_fraction((double)m_time_calibration/(double)(m_param_xml.nCalibratePeriod+5));
	  m_time_calibration++;
     }
     else if ((data.m_calibration) &&  (m_time_calibration>m_param_xml.nCalibratePeriod) && ( m_time_calibration<=m_param_xml.nCalibratePeriod+5))
     {
	  for (size_t i=0; i<data.num_Samples;i++)
	  {
	       if (sqrt(pow(data.data_out.RealOut[i],2)+pow(data.data_out.ImagOut[i],2))-m_calibration[i]>m_level)
	       {
		    m_level=sqrt(pow(data.data_out.RealOut[i],2)+pow(data.data_out.ImagOut[i],2))-m_calibration[i];
	       }
	  }
	  pgb_calibrate.set_fraction((double)m_time_calibration/(double)(m_param_xml.nCalibratePeriod+5));
	  m_time_calibration++;
     }
     else if (data.m_calibration &&  m_time_calibration>m_param_xml.nCalibratePeriod+5)
     {
	  StopRecord();
     }
}
double CSoundCard::Get_Level(void)
{
     return m_level;
}
s_data* CSoundCard::Get_Data()
{
     return &data;
}

bool CSoundCard::Is_Data_Freshed()
{
     return fresh_data;
}

out* CSoundCard::Get_Data_Out()
{
     fresh_data=false;
     return &data.data_out;
}

void CSoundCard::notify()
{
     m_Dispatcher.emit();
}

std::string CSoundCard::GetNameSndCard(PaDeviceIndex index_device)
{
     return Pa_GetDeviceInfo(index_device)->name;
}

void  CSoundCard::ScanListSndCard(void)
{
     const int Rates_to_test[] = {384000, 352800, 192000, 176400, 96000, 88200,
                                   48000,  44100,  32000,  24000,  22050, 16000,
				   15000,  12000,  11025,  9600,   8000};
     const size_t n_rate=sizeof(Rates_to_test)/sizeof(int);
     PaDeviceIndex n_Devices;
     n_Devices=Pa_GetDeviceCount();
     for(PaDeviceIndex i=0; i<n_Devices; i++)
     {
	  const PaDeviceInfo *deviceinfo;
	  deviceinfo=Pa_GetDeviceInfo(i);
	  std::string name_device(deviceinfo->name);
#ifdef __linux__
	  if (name_device.find("(hw:")!=std::string::npos)
	  {
#endif
	       if (deviceinfo->maxInputChannels!=0)
	       {
		    PaStreamParameters inputParameters;
		    std::vector<unsigned int> vec_order_snd_card;
		    v_namesndcards.push_back(name_device);
		    v_indexsndCards.push_back(i);
		    inputParameters.device=i;
		    inputParameters.channelCount=deviceinfo->maxInputChannels;
		    inputParameters.sampleFormat=paFloat32;
		    inputParameters.suggestedLatency=deviceinfo->defaultLowInputLatency;
		    inputParameters.hostApiSpecificStreamInfo=NULL;
		    for (size_t j=0; j<n_rate;j++)
		    {
			 if (Pa_IsFormatSupported(&inputParameters, nullptr, Rates_to_test[j])==paFormatIsSupported)
			 {
			      vec_order_snd_card.push_back(Rates_to_test[j]);
			 }
		    }
		    v_ratesndCards.push_back(vec_order_snd_card);
	       }
#ifdef __linux__
	  }
#endif
     }
}

void  CSoundCard::StopRecord()
{
     PaError error;
     if (stream!=nullptr)
     {
	  error=Pa_StopStream(stream);
	  if (error==paNoError)
	  {
	       error=Pa_CloseStream(stream);
	       stream=nullptr;
	       if (error!=paNoError)
	       {
		    ShowError(error);
	       }
	  }
	  else
	  {
	       ShowError(error);
	  }
     }
     if (m_WorkerThread)
     {
	  m_Worker.stop_work();
	  if (m_WorkerThread->joinable())
	  {
	       m_WorkerThread->join();
	  }
	  delete m_WorkerThread;
	  m_WorkerThread=nullptr;
     }
     if (m_wndSpectral)
     {
	  m_wndSpectral->hide();   //Close the spectral window
	  bSpectralDisplayed=false;
     }
     SupPointers_data();
     fresh_data=false;
     dlg_calibrate.hide();
}

//Configure the Sound Card
int CSoundCard::SetParameters(parameters &param)
{
     int err=0;
     const PaDeviceInfo *deviceinfo;
     deviceinfo=Pa_GetDeviceInfo(param.nNumber_sound_card);
     if (deviceinfo!=nullptr)
     {
	  if (deviceinfo->name==param.szName_sound_card)
	  {
	       param.period=deviceinfo->maxInputChannels;
	       Initialisation_Sound_Card(deviceinfo);
	  }
	  else
	  {
	       bool trouve=false;
	       size_t i=0;
	       while (i<v_namesndcards.size() && trouve==false)
	       {
		    if (v_namesndcards[i]==param.szName_sound_card)
		    {
			 param.nNumber_sound_card=i+1;
			 trouve=true;
		    }
		    i++;
	       }
	       if (trouve)
	       {
		    deviceinfo=Pa_GetDeviceInfo(param.nNumber_sound_card);
		    if (deviceinfo!=nullptr)
		    {
			 param.period=deviceinfo->maxInputChannels;
			 Initialisation_Sound_Card(deviceinfo);
		    }
	       }
	       else
	       {
		    err=-1;
	       }
	  }
	  inputParameters.device=param.nNumber_sound_card;
     }
     else
     {
	  err=-1;
     }
     m_param_xml=param;
     return err;
}

void CSoundCard::Initialisation_Sound_Card(const PaDeviceInfo *deviceinfo)
{
     inputParameters.channelCount=deviceinfo->maxInputChannels;
     inputParameters.sampleFormat=paFloat32;
     inputParameters.suggestedLatency=deviceinfo->defaultLowInputLatency;
     inputParameters.hostApiSpecificStreamInfo=NULL;
}
