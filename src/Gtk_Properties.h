// Gtk_Properties.h : Interface for the Gtk_Properties class
//
//////////////////////////////////////////////////////////////////////////
#ifndef PROPERTIES_H_INCLUDED
#define PROPERTIES_H_INCLUDED
#include <gtkmm.h>
#include <libxml/xmlwriter.h>
#include <libxml/xpath.h>
#include <fstream>
#include <sstream>
#include "Variables.h"
#include "Chord.h"
#include "SoundCard.h"
//class for the properties window
class Gtk_Properties : public Gtk::Dialog
{
    public:
     Gtk_Properties(BaseObjectType *cobject, const Glib::RefPtr<Gtk::Builder> & refGlade);  //Create the Gtk_Properties class
    ~Gtk_Properties();  //destructor
    int Load_Sound_Card_List(void);   //When the sound card is loaded
    int Chords_Load(std::string xml_file);    //When the chords are loaded
    void SetParameters(parameters param);   //Update the current parameters
    void SetPathParam_XML(std::string Path);    //Update the Parameters xml file path
    void SetPathShare(std::string Path);
     void Set_Sound_Card(CSoundCard *SoundCard); //Get the soundcard pointer
    protected:
    virtual void on_cboSoundCard_changed();  //When the sound card change
    virtual void on_sample_changed();    //When the numbers of samples change
    virtual void on_chkFrequency();
    virtual void on_Validate();  //When the validate button is clicked
    virtual void on_Cancel();  //When the cancel button is clicked
    virtual void on_cboGraphic_changed(); //When the the choice of the interface changed
    virtual void on_RadioChordClick();
    virtual void on_RadioNoteClick();
    virtual void on_RadioFrequencyClick();
    private:
     Glib::RefPtr<Gtk::Builder> m_refGlade; //Pointer on the glade file
     Gtk::ComboBox *m_cboChord, *m_cboGraphic, *m_cboNote ; //Pointer on four combos
     Gtk::ComboBoxText *m_cboFreqSample, *m_cboSoundCard; //Pointer for the combo of Frequency Samples
     Gtk::SpinButton *m_ctlSpin_Sample; // Samples numbers
  Gtk::SpinButton *m_ctlSpin_Octave;   //Sample frequency and octave
     Gtk::SpinButton *m_ctlSpin_Period;   //Sample of period to calibrate
     Gtk::SpinButton *m_ctlSpin_Persistence; //Persistence of the note
    Gtk::Button *m_btnValidate;  //Pointer on the button validate
    Gtk::Button *m_btnCancel;  //Pointer on the Cancel Button
    Gtk::SpinButton *m_ctlSpin_Attenuate;   
    Gtk::Entry *m_ctlEntry_Frequency; //Frequency to tune
    Gtk::CheckButton *m_ctl_CheckFrequency; //Display or not the spectral window
    Gtk::CheckButton *m_ctl_CheckMark;  //Display or not the Marks on the spectral window
    Gtk::RadioButton *m_ctlRadioChord, *m_ctlRadioNote, *m_ctlRadioFrequency; // radio button for the choices
    Gtk::Image *m_imgWindow;
    ModelColumns m_Columns_Chord,m_Columns_Notes;  //For the combos
    Glib::RefPtr<Gtk::ListStore> m_refTreeModel_Chord, m_refTreeModel_Notes; //Pointer ont the element list of the combos
    unsigned long int nPowerSample;   //Number of samples to save
#ifdef _WIN32
    unsigned long int nSampleFrequency; //Frequency of the samples
#endif
    parameters param_xml;   //xml parameters
    std::string szPath_param_xml; //Path for the xml parameters file
    std::string szPath_share; //Path for the share directory
    CSoundCard *SndCards;
    unsigned char Index_Tuner_Mode;
};
#endif // PROPRERTIES_H_INCLUDED
