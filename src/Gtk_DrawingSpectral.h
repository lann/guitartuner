// Gtk_DrawingSpectral.h : Interface for Gtk_DrawingSpectral class
//
////////////////////////////////////////////////////////////////////////////////////////////
#include <gtkmm.h>
#include "Chord.h"
#include "Variables.h"
#ifndef GTK_DRAWINGSPECTRAL_H_INCLUDED
#define GTK_DRAWINGSPECTRAL_H_INCLUDED
class Gtk_DrawingSpectral : public Gtk::DrawingArea
{
public : 
     Gtk_DrawingSpectral();  //create the class Gtk_DrawingSpectral
     ~Gtk_DrawingSpectral(); //The destructor
     void  SetParameters(CChord Chord, parameters param_xml, double *Imagout, double *Realout, double *cal); //Set the parameters for drawing the spectrum
     void SetChord(CChord Chord); //Update the chord to draw
     void UpdateMinMaxFrequency(void);  //Update the min and the max of the spectrum
     bool Get_Parameters_Validity(void);
protected :
     virtual bool on_scroll_event(GdkEventScroll *event); //Function to update the limit of the zoom
     virtual bool on_draw (const ::Cairo::RefPtr< ::Cairo::Context >& cr); //Function to update the window
private :
     CChord m_Chord;  //The chord to display
     parameters m_param_xml; //The parameters of the xml file
     double nMaxi;  //Freq maxi for the window
     double  nMini;  //Freq mini for the window
     double posX;  //Mouse position in x
     double dMiniZoom, dMaxiZoom;  //The frequency mini for a zoom
     double *m_Imagout, *m_Realout, *m_cal; //The frequency maxi for a zoom
     double zoom_x; //The value of the zoom
     bool m_Parameters_OK;
};
#endif
