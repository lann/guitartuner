// Gtk_Spectral.h : Interface for the Gtk_Spectral class
//
/////////////////////////////////////////////////////////////////////////
#include <gtkmm.h>
#include "Chord.h"
#include "Variables.h"
#include "Gtk_DrawingSpectral.h"
#ifndef GTK_SPECTRAL_H_INCLUDED
#define GTK_SPECTRAL_H_INCLUDED
class Gtk_Spectral : public Gtk::Window
{
public:
     Gtk_Spectral(); //create the class Gtk_Spectral
     ~Gtk_Spectral();    //The destructor
     Gtk_DrawingSpectral* GetDrawingAera(void); //Function return a Gtk_DrawingSpectral pointer
     void SetParameters(CChord Chord); //Set the paramaters for the Chord to tune
protected :
     virtual void on_cboNoteChoosen_changed(); //Function when the combo Chord item change
     virtual void on_btnResetZoom_clicked();  //Function for reset the zoom of the Gtk_DrawingSpectral
    
private :
     CChord m_Chord; //The chord to tune
     Gtk_DrawingSpectral m_FFTDisplay; //The Drawing spectrum
     Gtk::VBox vbox; 
     Gtk::ComboBox *m_cboNoteChoosen; //A pointer for the combo box Chord
     Gtk::ToolButton m_btnReset;   //A pointer for the button reset
     Gtk::SeparatorToolItem tool_separator;     
     Gtk::Toolbar m_toolbar;
     Gtk::ToolItem m_ItemCombo;    
     ModelColumns m_Columns_NoteChoosen;
     Glib::RefPtr<Gtk::ListStore> m_refTreeModel_NoteChoosen;
};
#endif
