#include "SoundCard.h"
#include "worker.h"
#include <chrono>

Worker::Worker() : m_Mutex(), m_shall_stop(false)
{

}

Worker::~Worker()
{
}

void Worker::stop_work()
{
     std::lock_guard<std::mutex> lock(m_Mutex);
     m_shall_stop=true;
}

void Worker::do_work(CSoundCard *caller)
{
  {
       std::lock_guard<std::mutex> lock(m_Mutex);
       data=caller->Get_Data();
       m_shall_stop=false;
  }
  {
       do
       {
	    std::this_thread::sleep_for(std::chrono::milliseconds(250));
            {
		 std::lock_guard<std::mutex> lock(m_Mutex);
		 fft.fft_float(data->num_Samples, 0, data->RealIn, data->ImagIn,
			       data->data_out.RealOut, data->data_out.ImagOut);
		 caller->notify();
            }
	    if (m_shall_stop)
	    {
		 break;
	    }
       } while (true);
  }
  caller->notify();
}
