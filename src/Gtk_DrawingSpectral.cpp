#include "Gtk_DrawingSpectral.h"
Gtk_DrawingSpectral::Gtk_DrawingSpectral() : m_Parameters_OK(false)
{
     add_events(Gdk::SCROLL_MASK); // add the event scroll for this widget
     zoom_x=1;  //Zoom to 1
     posX=0;  //Position of the mouse 0
}

Gtk_DrawingSpectral::~Gtk_DrawingSpectral()
{
}

//Refresh the window
bool Gtk_DrawingSpectral::on_draw (const ::Cairo::RefPtr< ::Cairo::Context >& cr)
{
    long int IndexStopMax,IndexStopMin;
    double height;
    double Freq_posX;
    height=0;
    Glib::RefPtr<Gdk::Window> win = get_window(); //Get the pointer of the window
    if ((win) && (m_Parameters_OK))
    {
        Gtk::Allocation allocation = get_allocation();
        const int win_width = allocation.get_width(); //Get the width and height of the window
        const int win_height = allocation.get_height();
        //All the area in white
        cr->save();
        cr->set_source_rgba(1, 1, 1, 0.9);
        cr->paint();
        cr->restore();
       
	//Create a type of line
	cr->save();
	cr->set_line_cap(Cairo::LINE_CAP_ROUND);
	cr->set_source_rgba(1, 0, 0, 0.9);
	//if one note in the chord
	if (m_Chord.GetNumberNote()==1)
	{
	     //Get the frequency min and max for the zoom
	     dMiniZoom=m_Chord.GetNote(0)->GetFrequency()-(m_Chord.GetNote(0)->GetFrequency()-nMini)/zoom_x;
	     dMaxiZoom=m_Chord.GetNote(m_Chord.GetNumberNote()-1)->GetFrequency()+m_Chord.GetNote(0)->GetFrequency()/zoom_x;
	}
	else if (posX!=0) //if the position of the mouse is not zero and the chord have some few notes
	{
	     Freq_posX=(posX/win_width)*(dMaxiZoom- dMiniZoom)+ dMiniZoom;
	     dMiniZoom=Freq_posX-(nMaxi-nMini)/zoom_x;
	     dMaxiZoom=Freq_posX+(nMaxi-nMini)/zoom_x;
	     posX=0;
	}
	if (dMiniZoom<0)
	     dMiniZoom=0;
	//Get the stop min and max for the frequency to display
	IndexStopMax=(long int)((dMaxiZoom*m_param_xml.nNumberSample)/m_param_xml.nFreqSampling);
        IndexStopMin=(long int)((dMiniZoom*m_param_xml.nNumberSample)/m_param_xml.nFreqSampling);
	//Get the max height of the spectrum
	for (long int i = IndexStopMin;((i < IndexStopMax) && (i < m_param_xml.nNumberSample)); i++)
	{
	     if (2*sqrt(pow(m_Realout[i], 2) + pow(m_Imagout[i], 2))/win_height>height)
	     {
		  height=2*sqrt(pow(m_Realout[i], 2) + pow(m_Imagout[i], 2))/win_height;
	     }
	}
	//Draw the frequencies of the spectrum
	for(long int i=IndexStopMin;((i<IndexStopMax) && (i<m_param_xml.nNumberSample));i++)
	{
	     double real, imaginary, amplitude;
	     real=m_Realout[i];
	     imaginary=m_Imagout[i];
	     amplitude=sqrt(pow(real, 2) + pow(imaginary, 2))-m_cal[i];
	     cr->set_source_rgba(1, 0, 0, 0.9);
	     cr->move_to(((i-IndexStopMin)*win_width)/(IndexStopMax-IndexStopMin),win_height);
	     cr->line_to((double)((i - IndexStopMin) * win_width /(IndexStopMax - IndexStopMin)),win_height - amplitude /height);
	     cr->stroke();
	}
//Draw the notes marker
	if (m_param_xml.bDispMark)
	{
	     cr->set_source_rgba(0,0,0,0.9);
	     for (int i=0; i<m_Chord.GetNumberNote(); i++)
	     {
		  double Note;
		  int long IndexNote;
		  int Marker;
		  std::string sznote;
		  Glib::RefPtr<Pango::Layout> pl=Pango::Layout::create(cr);
		  if (m_param_xml.sTunerMode<2)
		       sznote=m_Chord.GetNote(i)->GetNoteName();
		  else
		       sznote=m_Chord.GetNameChord();
		  pl->set_text(sznote);
		  Note=m_Chord.GetNote(i)->GetFrequency();
		  IndexNote=(long int) ((Note*m_param_xml.nNumberSample)/m_param_xml.nFreqSampling);
		  Marker=((IndexNote-IndexStopMin)*win_width)/(IndexStopMax-IndexStopMin);
		  cr->move_to(Marker,win_height);
		  cr->line_to((double)((IndexNote-IndexStopMin)*win_width/(IndexStopMax-IndexStopMin)),0);
		  cr->stroke();
	     }
             cr->restore();
        }
    }
    return true;
}

//Set the parameters for drawing the spectrum
void Gtk_DrawingSpectral::SetParameters(CChord Chord, parameters param_xml, double *Imagout, double *Realout, double *cal)
{
     m_Chord=Chord;
     m_param_xml=param_xml;
     m_Imagout=Imagout;
     m_Realout=Realout;
     m_cal=cal;
     
     if ((m_Realout!=nullptr) && (m_Imagout!=nullptr))
     {
	  m_Parameters_OK=true;
     }
     else
     {
	  m_Parameters_OK=false;
     }
     UpdateMinMaxFrequency();
}
//Set the chord to draw
void Gtk_DrawingSpectral::SetChord(CChord Chord)
{
     m_Chord=Chord;
     UpdateMinMaxFrequency();
}

//Update the min and the max of the spectrum
void Gtk_DrawingSpectral::UpdateMinMaxFrequency(void)
{
     //Get the minimum of the spectrum
     nMini=m_Chord.GetNote(0)->GetFrequency()/2;
     if (nMini<0)
	  nMini=0;
     //And the maximum
     nMaxi=m_Chord.GetNote(m_Chord.GetNumberNote()-1)->GetFrequency()+m_Chord.GetNote(0)->GetFrequency()-10;
     dMiniZoom=nMini;
     dMaxiZoom=nMaxi;
     zoom_x=1;
}

//Update the zoom when the mouse is scrolled
bool Gtk_DrawingSpectral::on_scroll_event(GdkEventScroll *event)
{
     posX=event->x;
    
     if ((event->direction==GDK_SCROLL_UP) && (zoom_x<1024))
     {
	  zoom_x*=2;
     }
     else if ((event->direction==GDK_SCROLL_DOWN) && (zoom_x>0.07))
     {
	  zoom_x/=2;
     }
     else
     {
	  posX=0;
     }
  
     return true;
}

bool Gtk_DrawingSpectral::Get_Parameters_Validity()
{
     return m_Parameters_OK;
}
