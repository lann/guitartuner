// Gtk_Tuner.h : Interface for the Gtk_Tuner class
//
/////////////////////////////////////////////////////////////////////////
#include <gtkmm.h>
#include <portaudio.h>
#include "Gtk_Properties.h"
#include <libxml/xmlreader.h>
#include <libxml/xpath.h>
#include "Chord.h"
#include "Variables.h"
#include <glibmm/thread.h>
#ifndef GTK_TUNER_H_INCLUDED
#define GTK_TUNER_H_INCLUDED
//Class for main window
class Gtk_Tuner : public Gtk::Window
{
public:
     Gtk_Tuner(const Glib::RefPtr<Gtk::Builder> & refGlade, PaError error);   //Create the Gtk_Tuner class
     ~Gtk_Tuner();   //Destuctor
     int AnalyseSignal(void);    //Thread for signal analysing
     void UpdateSlider(void);   //Update the note Slider
     void Load_Properties(void);   //Load the window  properties
     void SetPathChordXML(std::string szDirectory); //Update the directory of the chord xml file    
     Gtk::Allocation alloc; //Position for the index picture 
protected:
     virtual void onbuttonstop();   //Stop the analyse
     virtual void onbuttonanalyse();    //Analyse the  signal
     virtual void onproperties();    //Display the dialog box properties 
     virtual void onbuttoncalibrate(); //Calibrate the microphone
     virtual void on_wndGuitarTuner_hide();  //When Guitar Tuner stop
     virtual bool on_DrawingRound(const Cairo::RefPtr<Cairo::Context>& cr); //Update the round display
     virtual bool on_DrawingTuner(const Cairo::RefPtr<Cairo::Context>& cr); //Update the round display
     virtual void  on_Quit();    //When quit guitar tuner
     virtual void  on_menuProperties(); //When click on the menu properties
     virtual void on_menuAbout(); //When click about
private :
     CSoundCard soundcard;
     CChord Notes_Chord;  //All the chords
     std::vector<int> arsndCards;   //Vector for the index of the sound card
     Glib::RefPtr<Gtk::Builder> m_refGlade; //Pointer for the glade file
     //The containers 
     Gtk::VBox Vbox1,Vbox2,Vbox3;
     Gtk::HBox Hbox1,Hbox2,Hbox3,Hbox4,Hbox5,Hbox6;
//display parameters
     display_parameters param_display;
//For the menu and shortcut
     Glib::RefPtr<Gtk::Builder> m_refUIManager;
     Glib::RefPtr<Gio::SimpleActionGroup> m_refActionGroup;
//The controls
     Gtk::EventBox Eventbox[7];
     Gtk::Label lblNotes[6];
     Gtk::Image Pictures[7], m_pic_calibrate;
     Gtk::HScale hsLevel,hsAccurate;
     Glib::RefPtr< Gtk::Adjustment> adjustement,Adjustment_Accurate;
     Gtk::Statusbar stsInfo[5];
     unsigned ctx[5];

     Gtk::MenuBar m_menubar;
     Gtk::Menu m_menu_file, m_menu_settings, m_menu_about;
     Gtk::MenuItem m_menuitem_file, m_menuitem_edit, m_menuitem_help;
     Gtk::ImageMenuItem m_menu_image_quit, m_menu_image_settings, m_menu_image_about;
    
     Gtk::DrawingArea *m_Display; //The window for the round display and Tuner Display
     Gtk_Properties *m_dlgProperties;    //Pointer for the dialog box properties
     Gtk::AboutDialog *m_dlgAbout;   //Pointer for the dialog box About
     Gtk_Spectral *m_wndSpectral;  //Spectral Window
     Gtk::ToolButton *m_tbsettings,*m_tbanalyse, *m_tbcalibrate;
     int nPositionFirstIndex, nIntervalInterIndex; //Variables for positionning the index pictures
     Glib::RefPtr<Gtk::StyleContext> m_style;   //Label style
     sigc::connection pthread;  //Handle thread analyse
     double SearchPeak(void);  //Peak Search
     bool ValidatePeak(int IndexCentralFrequency); //Peak validation
     void ColorDisplay(int IndexLabel,Gdk::RGBA Color,bool bColor);   //Color display for the note label 
     void UpdateSliderAccurate(double Delta);  //Update the accurate slider
     void Load_parameters(std::string szFileParam);   //Load the file backup
     int SoundCard_changed(void);  //When the sound card change
     int Chord_changed(void);    //When the chord change
     void UpdateControls(void);    //Update the sound card and the label
     void Display_Standard(void);//Display the standard controls
     void Display_Round(void); //Display Round
     void Display_Tuner(void); //Display the tuner of the guitar
     void StartThread(void); //Start the thread for signal analysing
     void UpdateFrequency(void);//Update the note frequency
     void Change_Color_Widget(Glib::RefPtr<Gtk::StyleContext> style, Gdk::RGBA Color); //Change color of the widget background (EventBox)
     void Display_Drawing_Area(void);
     void Init_Search_Frequency(void); //Initialisation some variables for the frequency search
     void Get_Frequency_To_Displayed(double frequency); //Get the frequency to be displayed
     void Reset_Color_EventBox(Glib::RefPtr<Gtk::StyleContext> style, unsigned int index); //Remove color of the EventBox
     unsigned long int IndexSample;   //Samples entries play
     unsigned long int nPowerSample;   //All the sample entries
    
     bool bDisplaySpectral; //Displaying the spectral ?
     std::string m_szdirectory_chord_xml;  //Directory of chord xml file
     std::string m_szparameters_xml; // Directory of the guitar_tuner.xml file
     std::string m_szdirectory_share; //Directory of the pictures and icons
     std::string m_szHome; //Home directory for linux and windows
     std::string m_old_SoundCard; //The name of the sound card before to display the properties dialog
     unsigned int m_old_PeriodCalibrate; //The number of the period calibration before to display the properties dialog
     unsigned int m_old_Sample;
     double m_current_frequency_displayed; //The frequency displaying
     double m_current_frequency_detected; //The frequency detected in the last test
     unsigned int m_occ_frequency; //The number of occurrence of this frequency is detected
     parameters param_xml;   //parameters structure
     bool bSoundCard; //boolean for the sound card validity
     bool bChord;   //boolean for the Chord validity
     PaError m_error; //Variable to retrieve the portaudio error
     out *Data_Out;
     double *m_calibration;
};
#endif // GTK_TUNER_H_INCLUDED
