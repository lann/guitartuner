#include <gtkmm/main.h>
#include "Gtk_Tuner.h"
#include <iostream>
#include <libintl.h>
#define _(String) gettext (String)
#ifdef HAVE_CONFIG_H
  #include "config.h"
#endif
#ifdef _WIN32
#include <direct.h>
#endif

int main(int argc,char *argv[])
{
    Gtk::Main kit(argc,argv); //init the Gtk process
    Glib::RefPtr<Gtk::Builder> refXml;    //For the glade xml file
// Get the directory of the glade and xml files
    std::string directory_glade,directory_xml;
    PaError error; //Variable to retrieve the portaudio error
    setlocale (LC_ALL, "");
    
    #ifdef HAVE_CONFIG_H
    {
        directory_glade=GLADE_DIR;
        directory_xml=GLADE_DIR;
        directory_glade += "/GuitarTuner.glade";
        directory_xml+= "/accords.xml";
	bindtextdomain (PACKAGE, LOCALEDIR);
	bind_textdomain_codeset(PACKAGE, "UTF-8");
	textdomain (PACKAGE);
    }
    #else
    {
        char current_directory[PATH_MAX];
        getcwd(current_directory,PATH_MAX);
        std::string strDirectory(current_directory);
        directory_glade=(strDirectory+"/share/GuitarTuner.glade");
        directory_xml=(strDirectory+"/share/accords.xml");
	bindtextdomain("guitartuner","/usr/share/locale");
	bind_textdomain_codeset("guitartuner", "UTF-8");
	textdomain("guitartuner");
    }
    #endif
    #ifdef GLIBMM_EXCEPTIONS_ENABLED    //If Glib
    {
	std::string message(_("Problem to load the glade file"));
        try
        {
	     refXml=Gtk::Builder::create();
	     refXml->add_from_file(directory_glade); //create the xml Object
        }
        catch (const Glib::FileError &ex)    //If error
        {
            Gtk::MessageDialog Dialog(ex.what(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
            Dialog.set_title(message);
            Dialog.run();   // Display the error
            return 1;
        }
	catch(const Gtk::BuilderError& ex)
	{
            Gtk::MessageDialog Dialog(ex.what(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
            Dialog.set_title(message);
            Dialog.run();   // Display the error
            return 1;
	}
    }
    #else   //if not Glib
    {
	 std::auto_ptr<Gtk::Builder> error;
	 refXml=Gtk::Builder::create();
	 refXml->add_from_file(directory_glade); //create the xml Object
        if(error.get()) //If error
        {
            Gtk::MessageDialog Dialog(ex.what(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
            Dialog.set_title(message);
            Dialog.run();
            return 1;   //Display the error message
        }
    }
    #endif  //GLIBMM_EXCEPTIONS_ENABLED
    error=Pa_Initialize();
    Gtk_Tuner *pWindow=0;   //The Glade window
    pWindow=new Gtk_Tuner(refXml,error);
    if (pWindow)    //If the pointer exist
    {
        pWindow->SetPathChordXML(directory_xml);  //Update the xml directory
        pWindow->Load_Properties();   //Load the properties
        kit.run(*pWindow);  //Display the main window
    }
    delete pWindow;
    return 0;   //End
}
