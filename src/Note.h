// Note.h : interface for the CNote class
// CNote represent a Note with :
// - his name (E1 .....)
// - his frequency 
// - his validity
/////////////////////////////////////////////////////////////////
#ifndef CNOTE_H_INCLUDED
#define CNOTE_H_INCLUDED
#include <iostream>
#include <vector>
#include "Variables.h"
//Class CNote

class CNote
{
public:
     CNote();
     ~CNote();
     CNote(std::string NoteName);  //create the class with the note name
     CNote(double Frequency);
     std::string GetNoteName(void); //return the note's name
     void SetNoteName(std::string NoteName); //set the name of the note
     double GetFrequency(void);  //return the frequency of the note
     void SetFrequency(double Frequency); //set the frequency of the note
     bool GetValidity(void); //return the validity of the note
     double GetFrequencyAbove(void); //Return the frequency of the note above
     double GetFrequencyBelow(void); //Return the frequency of the note below
private:
     static const double m_fNoteStandard[17];	//The frequencies of the scale
     std::string m_szNoteName;  //the name of the note
     double m_FreqNote; //the frequency of the note
     double m_Freq_above; //The frequency of the note above
     double m_Freq_below; //The frequency of the note below
     bool m_bvalid;  //the validity of the note
     void Initialisation();	//variable initialisation
     void CalculateNote(void); //Calculate the frequency of the note
     void Calculate_String_Note_Name(void); //Calculate, with his frequency, the name of the near note
};
#endif
