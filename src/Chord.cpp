#include <math.h>
#include "Chord.h"
#include <stdlib.h>
#include <string.h>
#include <libintl.h>
#include "Note.h"
#define _(String) gettext (String)

CChord::CChord()
{
     m_bvalid=true;
}

CChord::~CChord()
{
}

CChord::CChord(std::string NameChord)
{
	m_szNameChord=NameChord;	//Update the name of the chord
	 m_bvalid=true;
}

void CChord::SetChord(std::string NameChord)
{
	m_szNameChord=NameChord;	//Update the name of the chord
}

std::string CChord::GetNameChord()
{
	return m_szNameChord;	//Return the name of the chord
}

int CChord::GetNoteBel(double Freq)
{
     //Return the index of the note in the chord immediatly below (-1 if the note is lowest and 5 if the note is highest
	int IndexNote;	//Index of the note
	int i=0;
	while ((i<m_Notes.size()) && (m_Notes.at(i).GetFrequency()<Freq))
		i++;
	IndexNote=i-1;	//Minus 1 
	return IndexNote;	//Return the index
}

int CChord::GetNoteAbo(double Freq)
{
     //Return the index of the note in the chord immedialtly above (6 if the note is highest or 0 if the note is belowest)
	int IndexNote;  //Index of the note
	int i=0;
	while ((i<m_Notes.size()) && (m_Notes.at(i).GetFrequency()<Freq))
		i++;
	IndexNote=i;
	return IndexNote;	//Return the index of the note
}

void CChord::AddNote(CNote Note)
{
     m_Notes.push_back(Note); 
     if (Note.GetValidity()==false)
	 m_bvalid=false;
}

void CChord::DelNote(int Index)
{
     m_Notes.erase(m_Notes.begin()+Index-1);
}

CNote* CChord::GetNote(int Index)
{
     return &m_Notes.at(Index);
}

bool CChord::GetValidity()
{
     return m_bvalid;
}

void CChord::ClearNotes(void)
{
     m_Notes.clear();
}

int CChord::GetNumberNote()
{
     return m_Notes.size();
}
