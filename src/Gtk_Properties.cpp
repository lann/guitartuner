#include "Gtk_Properties.h"
#include <libintl.h>
#include <glibmm/ustring.h>
#define _(String) gettext (String)
#ifdef _WIN32
#include <windows.h>
#include <mmsystem.h>
#endif //_WIN32

//Constructeur
Gtk_Properties::Gtk_Properties(BaseObjectType *cobject, const Glib::RefPtr<Gtk::Builder> & refGlade):Gtk::Dialog(cobject),
												     m_refGlade(refGlade)
{
     m_refGlade->get_widget("combobox3",m_cboChord);    //for the combos
     m_refGlade->get_widget("combobox1",m_cboSoundCard);   //and the others controls
     m_refGlade->get_widget("spinbutton4",m_ctlSpin_Sample);
     m_refGlade->get_widget("combobox2",m_cboFreqSample);
     m_refGlade->get_widget("button1",m_btnValidate);
     m_refGlade->get_widget("button2",m_btnCancel);
     m_refGlade->get_widget("checkbutton2",m_ctl_CheckFrequency);
     m_refGlade->get_widget("spinbutton3",m_ctlSpin_Attenuate);
     m_refGlade->get_widget("combobox4",m_cboGraphic);
     m_refGlade->get_widget("image1",m_imgWindow);
     m_refGlade->get_widget("checkbutton1",m_ctl_CheckMark);
     m_refGlade->get_widget("radiobutton1",m_ctlRadioChord);
     m_refGlade->get_widget("radiobutton2",m_ctlRadioNote);
     m_refGlade->get_widget("radiobutton3",m_ctlRadioFrequency);
     m_refGlade->get_widget("combobox5",m_cboNote);
     m_refGlade->get_widget("spinbutton1",m_ctlSpin_Octave);
     m_refGlade->get_widget("spinbutton5", m_ctlSpin_Period);
     m_refGlade->get_widget("spinbutton6", m_ctlSpin_Persistence);
     m_refGlade->get_widget("entry1",m_ctlEntry_Frequency);
     if (m_cboNote)
     {
	  m_refTreeModel_Notes = Gtk::ListStore::create(m_Columns_Notes);
	  m_cboNote->set_model(m_refTreeModel_Notes);
	  Gtk::TreeModel::Row row;
	  for(int i=0; i<18; i++)
	  {
	       row=*(m_refTreeModel_Notes->append());
	       row[m_Columns_Notes.m_col_id]=i;
	       row[m_Columns_Notes.m_col_name]=Name_Note[i];
	  }	 
	 
	  m_cboNote->pack_start(m_Columns_Notes.m_col_name);
     }
     if (m_cboGraphic)
     {
	  m_cboGraphic->signal_changed().connect(sigc::mem_fun(*this,&Gtk_Properties::on_cboGraphic_changed));
     }
     if (m_cboSoundCard)  //Create the changed signal for the sound card combo and the sound card list model
     {
	  m_cboSoundCard->signal_changed().connect(sigc::mem_fun(*this,&Gtk_Properties::on_cboSoundCard_changed));
     }
     if (m_ctlSpin_Sample)  //Create the change value signal for the Spin sample
     {
	  nPowerSample=m_ctlSpin_Sample->get_value();
	  m_ctlSpin_Sample->signal_value_changed().connect(sigc::mem_fun(*this,&Gtk_Properties::on_sample_changed));
     }
     if (m_cboChord)    //If the combo pointer exist
     {
	  m_refTreeModel_Chord = Gtk::ListStore::create(m_Columns_Chord);   //Create the list model
	  m_cboChord->set_model(m_refTreeModel_Chord);  //Update the combo with this list
     }
     if (m_btnValidate)   //Create the click signal for the Button Validate 
     {
	  m_btnValidate->signal_clicked().connect(sigc::mem_fun(*this,&Gtk_Properties::on_Validate));
     }
     if (m_btnCancel)   //And the Cancel button
     {
	  m_btnCancel->signal_clicked().connect(sigc::mem_fun(*this,&Gtk_Properties::on_Cancel));
     }
     if (m_ctl_CheckFrequency)
     {
	  m_ctl_CheckFrequency->signal_clicked().connect(sigc::mem_fun(*this,&Gtk_Properties::on_chkFrequency));
     }
     if (m_ctlRadioChord) //For the choice button of the tuner mode
     {
	  Gtk::RadioButton::Group group=m_ctlRadioChord->get_group();  //Get the group of all the Radio controls
	  m_ctlRadioChord->signal_clicked().connect(sigc::mem_fun(*this,&Gtk_Properties::on_RadioChordClick));  //Create the signal click for the first
	  if (m_ctlRadioNote)  //and the next 
	  {
	       m_ctlRadioNote->set_group(group);
	       m_ctlRadioNote->signal_clicked().connect(sigc::mem_fun(*this,&Gtk_Properties::on_RadioNoteClick));
	  }
	  if (m_ctlRadioFrequency)
	  {
	       m_ctlRadioFrequency->set_group(group);
	       m_ctlRadioFrequency->signal_clicked().connect(sigc::mem_fun(*this,&Gtk_Properties::on_RadioFrequencyClick));
	  }
     }
#ifdef _WIN32
     set_size_request(500,300);
#endif
}

//Destructor
Gtk_Properties::~Gtk_Properties()
{

}

void Gtk_Properties::Set_Sound_Card(CSoundCard *SoundCard)
{
     SndCards=SoundCard;
}

// When click on the radio chord
void Gtk_Properties::on_RadioChordClick()
{
     m_cboChord->set_sensitive(true);
     m_cboNote->set_sensitive(false);
     m_ctlSpin_Octave->set_sensitive(false);
     m_ctlEntry_Frequency->set_sensitive(false);
     m_ctlEntry_Frequency->set_editable(false);
     Index_Tuner_Mode=0;
}

// When click on the radio Note
void Gtk_Properties::on_RadioNoteClick()
{
     m_cboChord->set_sensitive(false);
     m_cboNote->set_sensitive(true);
     m_ctlSpin_Octave->set_sensitive(true);
     m_ctlEntry_Frequency->set_sensitive(false);
     m_ctlEntry_Frequency->set_editable(false);
     Index_Tuner_Mode=1;
}

//When click on the radio Frequency
void Gtk_Properties::on_RadioFrequencyClick()
{
     m_cboChord->set_sensitive(false);
     m_cboNote->set_sensitive(false);
     m_ctlSpin_Octave->set_sensitive(false);
     m_ctlEntry_Frequency->set_sensitive(true);
     m_ctlEntry_Frequency->set_editable(true);
     Index_Tuner_Mode=2;
}

// When the display change
void Gtk_Properties:: on_cboGraphic_changed()
{
     std::string picture;
     switch (m_cboGraphic->get_active_row_number())
     {
     case 0:
#ifdef __linux__
	  picture=szPath_share+"/Affichage_Normal.png";
#elif defined _WIN32
	  picture=szPath_share+"\\Affichage_Normal.png";
#endif
	  break;
     case 1:
#ifdef __linux__
	  picture=szPath_share+"/Affichage_rond.png";
#elif defined _WIN32
	  picture=szPath_share+"\\Affichage_rond.png";
#endif
	  break;
     case 2:
#ifdef __linux__
	  picture=szPath_share+"/Affichage_tuner.png";
#elif defined _WIN32
	  picture=szPath_share+"\\Affichage_tuner.png";
#endif
	  break;
     }
     m_imgWindow->set(picture);
}

//When a item of the sound card combo change
void Gtk_Properties::on_cboSoundCard_changed(void)
{
     int CardItem; //The index of the item
     std::vector<unsigned int> vec_nRates;
     CardItem=m_cboSoundCard->get_active_row_number();
     vec_nRates=SndCards->GetListRateSndCard(CardItem);
     m_cboFreqSample->remove_all();
     for (size_t i=0; i<vec_nRates.size();i++)
     {
	  std::ostringstream ostr;
	  ostr<<vec_nRates.at(i);
	  m_cboFreqSample->append(ostr.str());
     }
     m_cboFreqSample->set_active(0);
}

//Loading the sound cards 
int Gtk_Properties::Load_Sound_Card_List(void)
{
     std::vector<std::string> namesndcards;
     int ret=1;
     namesndcards=SndCards->GetListSndCard();
     if (namesndcards.size()>0)
     {
	  m_cboSoundCard->remove_all();
	  for (int i=0; i<namesndcards.size();i++)
	  {
	       //Update the sound card combo
	       m_cboSoundCard->append(namesndcards.at(i));
	  }
     }
     else
     {
	  std::string message(_("No sound card\n"));
	  message+=_("The software stopped");
	  Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
	  Dialog.set_title(_("Sound card detection problem"));
	  Dialog.run();
	  ret=-1;
     }
     return ret;
}

//Loading the chord file
int Gtk_Properties::Chords_Load(std::string xml_file)
{
     int ret,IndexLine=1;
     CChord Notes_Chords;  //Get the chords
     char *nameChord;    //Pointer for the name of the chord
     xmlDocPtr xmlparameters_doc = NULL; //Document pointer
     xmlXPathContextPtr xmlparameters_context = NULL;    //Pointer fot the xml file context
     //Pointer for the xml file objects
     xmlXPathObjectPtr xmlobject_chord,xmlobject_note,xmlobject_nameChord, xmlobject_IndexNote, xmlobject_Heigth;
     xmlNodePtr node;    //The current node
     ret=-1;
     //Get the pointer of the document
     xmlparameters_doc = xmlParseFile (xml_file.c_str());
     if (xmlparameters_doc)
     {
	  //Init the path
	  xmlXPathInit ();
	  // Get the context
	  xmlparameters_context = xmlXPathNewContext (xmlparameters_doc);
	  if (xmlparameters_context)
	  {
	       //Get all objects /accords
	       xmlobject_chord = xmlXPathEval (BAD_CAST "/accords/text()", xmlparameters_context);
	       //For all the chords
	       for (int IndexChord=1;IndexChord<xmlobject_chord->nodesetval->nodeNr;IndexChord++)
	       {
		    std::string path_note("/accords/accord");
		    std::string path_name_chord,path_temp;
		    std::ostringstream Number;
		    Number<<IndexChord;
		    path_note+=Number.str();
		    path_temp=path_note;
		    //Update the paths
		    path_name_chord=path_note + "/nom/text()";
		    path_note+="/text()";
		    //Get the objects of the chord notes
		    xmlobject_note = xmlXPathEval ((const xmlChar*)path_note.c_str(), xmlparameters_context);
		    xmlobject_nameChord = xmlXPathEval ((const xmlChar*)path_name_chord.c_str(), xmlparameters_context);
		    node=xmlobject_nameChord->nodesetval->nodeTab[0];
		    //Get the name of the chord
		    nameChord=(char*)node->content;
		    //Update the chord
		    Notes_Chords.SetChord(nameChord);
		    //For all notes in the chord
		    for (int IndexNote=1; IndexNote<xmlobject_note->nodesetval->nodeNr-1;IndexNote++)
		    {
			 std::string path_name_note(path_temp), path_Index_note, path_Heigth_Note;
			 std::ostringstream stream_o;
			 path_name_note+="/note";
			 stream_o<<IndexNote;    
			 path_name_note+=stream_o.str();
			 //Update the path for a note
			 path_Index_note=path_name_note+"/Index_Note/text()";
			 path_Heigth_Note=path_name_note+"/Hauteur/text()";
			 // Get the note name of the object
			 xmlobject_IndexNote = xmlXPathEval ((const xmlChar*)path_Index_note.c_str(), xmlparameters_context);
			 xmlobject_Heigth=xmlXPathEval((const xmlChar*)path_Heigth_Note.c_str(),xmlparameters_context);
			 if (xmlobject_IndexNote->nodesetval->nodeNr && xmlobject_Heigth->nodesetval->nodeNr)
			 {
			      int nIndex_Note;
			      std::string szNameNote;
			      // Get the index note of the node
			      node=xmlobject_IndexNote->nodesetval->nodeTab[0];
			      const std::string szIndex_Note((const char*)node->content);
			      std::istringstream issIndex_Note(szIndex_Note);
	    
			      issIndex_Note>>nIndex_Note;
			      node=xmlobject_Heigth->nodesetval->nodeTab[0];
			      szNameNote=Name_Note[nIndex_Note] + (const char*)node->content;
			
			      CNote Note(szNameNote);
			      //Update the CAccord Object
			      Notes_Chords.AddNote( Note);
			 }
		    }
		    //If the chord is not valid
		    if  (Notes_Chords.GetValidity()==false)
		    {
			 std::ostringstream out;
			 out<<IndexChord;
			 Glib::ustring message=Glib::ustring::compose(_("The chord number %1 has a bad format"),out.str());
                       
			 Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_WARNING ,Gtk::BUTTONS_OK ,false );
			 Dialog.set_title(_("Chord format problem"));
			 Dialog.run();
		    }
		    else
		    {
			 // Save the chord and update the combo
			 Gtk::TreeModel::Row row = *(m_refTreeModel_Chord->append());
			 row[m_Columns_Chord.m_col_id]=IndexLine;
			 row[m_Columns_Chord.m_col_name]=Notes_Chords.GetNameChord();
			 IndexLine++;
			 ret=1;
		    }
	       }
	       //Free the pointers
	       xmlXPathFreeObject(xmlobject_chord);
	       xmlXPathFreeObject(xmlobject_note);
	       xmlXPathFreeObject(xmlobject_nameChord);
	       xmlXPathFreeObject(xmlobject_IndexNote);
	       xmlXPathFreeObject(xmlobject_Heigth);
	       xmlXPathFreeContext(xmlparameters_context);
	  }
	  else
	  {
	       std::string message(_("Problem to create a xml context"));
	       Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
	       Dialog.set_title(_("XML Problem"));
	       Dialog.run();
	       ret=-1;
	  }
	  xmlFreeDoc(xmlparameters_doc);
	  m_cboChord->pack_start(m_Columns_Chord.m_col_name);
     }
     //Choose the file int dialog box
     else
     {
	  Glib::ustring message=Glib::ustring::compose(_("Problem to open the chords file %1"),xml_file);
        
	  Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
	  Dialog.set_title(_("Problem chords file"));
	  Dialog.run();
	  //Window to choose the file
	  Gtk::FileChooserDialog ChooseFile(_("Choose the file"),Gtk::FILE_CHOOSER_ACTION_OPEN);
	  ChooseFile.set_transient_for(*this);
	  //Add the buttons in the window
	  ChooseFile.add_button(Gtk::Stock::CANCEL, Gtk::RESPONSE_CANCEL);
	  ChooseFile.add_button(Gtk::Stock::OPEN, Gtk::RESPONSE_OK);
	  auto filter = Gtk::FileFilter::create();
	  filter->set_name(_("XML files"));
	  filter->add_pattern("*.xml");
	  ChooseFile.add_filter(filter);
	  int result=ChooseFile.run();
	  if (result==Gtk::RESPONSE_OK)
	  {
	       ret=Chords_Load(ChooseFile.get_filename());
	  }
	  else
	       ret=-1;
     }
     return ret;
}

//Changing the samples quantity 
void Gtk_Properties::on_sample_changed()
{
     Glib::RefPtr<Gtk::Adjustment> range;
     range=m_ctlSpin_Sample->get_adjustment();
     if (range)
     {
	  //If the samples quantity down
	  if (nPowerSample>range->get_value())
	  {
	       //Value/2
	       range->set_value(nPowerSample/2);
	       //An too for the increment
	       range->set_step_increment(nPowerSample/2);
	  }
	  else if (nPowerSample<range->get_value())
	  {
	       //Else the increment is in the new value
	       range->set_step_increment(range->get_value());
	  }
	  //Save the quantity of the samples
	  nPowerSample=range->get_value();
     }
}

//Parameters validation
void Gtk_Properties::on_Validate(void)
{
//Create the file 
     std::ofstream file_xml(szPath_param_xml.c_str(), std::ios::out | std::ios::trunc);
     std::string title(_("Writing problem"));
     std::istringstream stream;
     if (file_xml)
     {
	  int result;
	  std::istringstream istr;
	  xmlTextWriterPtr writer;    //Pointer on the file
	  Gtk::TreeModel::iterator iter;
	
	  file_xml.close();    //Close the file
	  //Get the parameters
	  istr.str(m_cboFreqSample->get_active_text());
	  istr>>param_xml.nFreqSampling;
	  param_xml.nNumberChord=m_cboChord->get_active_row_number();
	  param_xml.nNumber_sound_card=SndCards->GetListIndexSndCard().at(m_cboSoundCard->get_active_row_number());
	  param_xml.szName_sound_card=m_cboSoundCard->get_active_text();
	  param_xml.nNumberSample=m_ctlSpin_Sample->get_value();
	  param_xml.bDispFreq=m_ctl_CheckFrequency->get_active();
	  param_xml.bDispMark=m_ctl_CheckMark->get_active();
	  param_xml.nAttenuation= m_ctlSpin_Attenuate->get_value();
	  param_xml.nDisplay_Interface=m_cboGraphic->get_active_row_number();
	  iter=m_cboNote->get_active();
	  if (iter)
	  {
	       Gtk::TreeModel::Row row=*iter;
	       if (row)
	       {
		    param_xml.szNote=row[m_Columns_Notes.m_col_name];
	       }
	  }
	  param_xml.sOctave=m_ctlSpin_Octave->get_value();
	  param_xml.nCalibratePeriod=m_ctlSpin_Period->get_value();
	  param_xml.Persistence_Note_Delay=m_ctlSpin_Persistence->get_value();
	  stream.str(m_ctlEntry_Frequency->get_text());
	  stream>>param_xml.dFrequency;
	  param_xml.sTunerMode=Index_Tuner_Mode;
	  param_xml.sIndexNote=m_cboNote->get_active_row_number();
	  //Get the pointer of xml file
	  writer = xmlNewTextWriterFilename(szPath_param_xml.c_str(), 0);
	  if (writer)
	  {
	       //Header
	       result = xmlTextWriterStartDocument(writer, NULL, NULL, NULL);
	       if (result!=-1)
	       {
		    //Create node PARAMETRES
		    result = xmlTextWriterStartElement(writer, BAD_CAST "PARAMETRES");
		    if (result!=-1)
		    {
			 //Create node header
			 result = xmlTextWriterStartElement(writer, BAD_CAST "HEADER");
			 if (result!=-1)
			 {
			      //Create node Visualisation_Spectrale and his value
			      result = xmlTextWriterWriteFormatElement(writer, BAD_CAST "Visualisation_Spectrale","%d", param_xml.bDispFreq);
			      if (result!=-1)
			      {
				   //same thing for the others parameters
				   result = xmlTextWriterWriteFormatElement(writer, BAD_CAST "Frequence_Echantillonnage","%d", param_xml.nFreqSampling);
				   if (result!=-1)
				   {
					result = xmlTextWriterWriteFormatElement(writer, BAD_CAST "Nombre_Echantillon","%d", param_xml.nNumberSample);
					if (result!=-1)
					{
					     result=xmlTextWriterWriteFormatElement(writer, BAD_CAST "Numero_Accord","%d", param_xml.nNumberChord);
					     if (result!=-1)
					     {
						  result=xmlTextWriterWriteFormatElement(writer, BAD_CAST "Numero_Carte_Son","%d", param_xml.nNumber_sound_card);
						  if (result!=-1)
						  {
						       result=xmlTextWriterWriteFormatElement(writer, BAD_CAST "Nom_Carte_Son","%s", param_xml.szName_sound_card.c_str());
						       if (result!=-1)
						       {
							    result=xmlTextWriterWriteFormatElement(writer, BAD_CAST "Valeur_Attenuation","%d", param_xml.nAttenuation);
							    if (result!=-1)
							    {
								 result=xmlTextWriterWriteFormatElement(writer,BAD_CAST "Interface_Graphique","%d",param_xml.nDisplay_Interface);
								 if (result!=-1)
								 {
								      result=xmlTextWriterWriteFormatElement(writer,BAD_CAST "Frequence_Repere","%d",param_xml.bDispMark);
								      if (result!=-1)
								      {
									   result=xmlTextWriterWriteFormatElement(writer,BAD_CAST "Note","%s",param_xml.szNote.c_str());
									   if (result!=-1)
									   {
							     
										result=xmlTextWriterWriteFormatElement(writer,BAD_CAST "Octave","%d",param_xml.sOctave);
										if (result!=-1)
										{
										     result=xmlTextWriterWriteFormatElement(writer,BAD_CAST "Frequence","%f",param_xml.dFrequency);
										     if (result!=-1)
										     {
											  result=xmlTextWriterWriteFormatElement(writer,BAD_CAST "Mode_Accordeur","%d",param_xml.sTunerMode);
											  if (result!=-1)
											  {
											       result=xmlTextWriterWriteFormatElement(writer,BAD_CAST "IndexNote","%d",param_xml.sIndexNote);
											       if (result!=-1)
											       {
												    result=xmlTextWriterWriteFormatElement(writer,BAD_CAST "Periode_Calibration","%d",param_xml.nCalibratePeriod);
												    if (result!=-1)
												    {
													 result=xmlTextWriterWriteFormatElement(writer,BAD_CAST "Persistence","%d",param_xml.Persistence_Note_Delay);
													 if (result!=-1)
													 {
													      result = xmlTextWriterEndElement(writer);
													      if (result!=-1)
													      {
														   result = xmlTextWriterEndDocument(writer);
														   if (result==-1)
														   {
															std::string message(_("Cannot close the document"));
															Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
															Dialog.set_title(title);
															Dialog.run();
														   }
													      }
													 }
													 else
													 {
													      Glib::ustring message=Glib::ustring::compose(_("Cannot write the element %1"),"Persistence");
													      Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
													      Dialog.set_title(title);
													      Dialog.run();
													 }
												    }
												    else
												    {
													 Glib::ustring message=Glib::ustring::compose(_("Cannot write the element %1"),"Periode_Calibration");
													 Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
													 Dialog.set_title(title);
													 Dialog.run();
												    }
											       }
											       else
											       {
												    //std:string end_message("IndexNote")
												    Glib::ustring message=Glib::ustring::compose(_("Cannot write the element %1"),"IndexNote");
										  
												    Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
												    Dialog.set_title(title);
												    Dialog.run();
											       }
											  }
											  else
											  {
											       Glib::ustring message=Glib::ustring::compose(_("Cannot write the element %1"),"Mode_Accordeur");
											       Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
											       Dialog.set_title(title);
											       Dialog.run();
											  }
										     }
										     else
										     {
											  Glib::ustring message=Glib::ustring::compose(_("Cannot write the element %1"),"Frequence");
											  Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
											  Dialog.set_title(title);
											  Dialog.run();
										     }
										}
										else
										{
										     Glib::ustring message=Glib::ustring::compose(_("Cannot write the element %1"),"Octave");
										     Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
										     Dialog.set_title(title);
										     Dialog.run();
										}
									   }
									   else
									   {
										Glib::ustring message=Glib::ustring::compose(_("Cannot write the element %1"),"Note");
										Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
										Dialog.set_title(title);
										Dialog.run();
									   }
								      }
						    
								      else
								      {
									   Glib::ustring message=Glib::ustring::compose(_("Cannot write the element %1"),"Visualisation repère");   
									   Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
									   Dialog.set_title(title);
									   Dialog.run();
								      }
								 }
								 else
								 {
								      Glib::ustring message=Glib::ustring::compose(_("Cannot write the element %1"),"interface graphique");   	  
								      Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
								      Dialog.set_title(title);
								      Dialog.run();
								 }
							    }
							    else
							    {
								 Glib::ustring message=Glib::ustring::compose(_("Cannot write the element %1"),"Valeur_Attenuation");
								 Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
								 Dialog.set_title(title);
								 Dialog.run();
							    }
						       }
						       else
						       {
							    Glib::ustring message=Glib::ustring::compose(_("Cannot write the element %1"),"Nom Carte Son");
							    Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
							    Dialog.set_title(title);
							    Dialog.run();
			
						       }
						  }
						  else
						  {
						       Glib::ustring message=Glib::ustring::compose(_("Cannot write the element %1"),"Numero_Carte_Son");
						       Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
						       Dialog.set_title(title);
						       Dialog.run();
						  }
					     }
					     else
					     {
						  Glib::ustring message=Glib::ustring::compose(_("Cannot write the element %1"),"Numero_Accord");
						  Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
						  Dialog.set_title(title);
						  Dialog.run();
					     }
					}
					else
					{
					     Glib::ustring message=Glib::ustring::compose(_("Cannot write the element %1"),"Nombre_Echantillon");
					     Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
					     Dialog.set_title(title);
					     Dialog.run();
					}
				   }
				   else
				   {
					Glib::ustring message=Glib::ustring::compose(_("Cannot write the element %1"),"Frequence_Echantillonnage");
					Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
					Dialog.set_title(title);
					Dialog.run();
				   }
			      }
			      else
			      {
				   Glib::ustring message=Glib::ustring::compose(_("Cannot write the element %1"),"Visualisation_Spectrale");
				   Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
				   Dialog.set_title(title);
				   Dialog.run();
			      }
			 }
			 else
			 {
			      Glib::ustring message=Glib::ustring::compose(_("Cannot write the element %1"),"HEADER");
			      Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
			      Dialog.set_title(title);
			      Dialog.run();
			 }
		    }
		    else
		    {
			 std::string message(_("Cannot write the first element"));
			 Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
			 Dialog.set_title(title);
			 Dialog.run();
		    }
	       }
	       else
	       {
		    std::string message(_("Cannot write to the xml document"));
		    Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
		    Dialog.set_title(title);
		    Dialog.run();
	       }
	       xmlFreeTextWriter(writer);
	  }
	  else
	  {
	       Glib::ustring message=Glib::ustring::compose(_("Cannot create the object %1"),"WRITER");
	       Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
	       Dialog.set_title(_("Create object problem"));
	       Dialog.run();
	  }
     }
     else
     {
	  Glib::ustring message=Glib::ustring::compose(_("Cannot create the %1 file"),"guitar_tuner.xml");
	  Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
	  Dialog.set_title(_("Create settings file problem"));
	  Dialog.run();
     }
     hide(); // Close the dialog box
}

// When click on cancel
void Gtk_Properties::on_Cancel(void)
{
     hide(); //Close the dialog box
}

//Update the parameters and the controls
void Gtk_Properties::SetParameters(parameters param)
{
     std::ostringstream ostr;
     std::string name_sound_card;
     param_xml=param;
     ostr<<param_xml.nFreqSampling;
     m_cboFreqSample->set_active_text(ostr.str());
     m_cboChord->set_active(param_xml.nNumberChord);
     name_sound_card=SndCards->GetNameSndCard(param_xml.nNumber_sound_card);
     m_cboSoundCard->set_active_text(name_sound_card);
     m_ctlSpin_Sample->set_value(param_xml.nNumberSample);
     m_ctlSpin_Sample->set_increments(param_xml.nNumberSample,param_xml.nNumberSample);
     m_ctl_CheckFrequency->set_active(param_xml.bDispFreq);
     on_chkFrequency();
     m_ctlSpin_Attenuate->set_value(param_xml.nAttenuation);
     m_cboGraphic->set_active(param_xml.nDisplay_Interface);
     m_ctl_CheckMark->set_active(param_xml.bDispMark);
     m_cboNote->set_active(param_xml.sIndexNote);
     m_ctlSpin_Octave->set_value(param_xml.sOctave);
     m_ctlSpin_Period->set_value(param_xml.nCalibratePeriod);
     m_ctlSpin_Persistence->set_value(param_xml.Persistence_Note_Delay);
     m_ctlEntry_Frequency->set_text(Glib::ustring::format(param_xml.dFrequency));
     Index_Tuner_Mode=param_xml.sTunerMode;
     switch (Index_Tuner_Mode)
     {
     case 0:
	  m_ctlRadioChord->activate();
	  m_ctlRadioChord->clicked();
	  break;
     case 1:
	  m_ctlRadioNote->activate();
	  m_ctlRadioNote->clicked();
	  break;
     case 2:
	  m_ctlRadioFrequency->activate();
	  m_ctlRadioFrequency->clicked();
	  break;
     }
     on_Validate();
     on_cboGraphic_changed();
}

// Update the name of the path of the parameters file
void Gtk_Properties::SetPathParam_XML(std::string Path)
{
     szPath_param_xml=Path;
}

//Update the name of the path of the share file
void Gtk_Properties::SetPathShare(std::string Path )
{
     szPath_share=Path;
}

// When the spectral window is checked
void Gtk_Properties::on_chkFrequency()
{
     if (m_ctl_CheckFrequency->get_active())
// if the window is active, the note markers are checkable
	  m_ctl_CheckMark->set_sensitive(true);
     else
     {
//Else nont checkable and no activate
	  m_ctl_CheckMark->set_sensitive(false);
	  m_ctl_CheckMark->set_active(false);
     }
}
