#include "Gtk_Tuner.h"
#include "Gtk_Properties.h"
#include "FFT.h"
#include <libintl.h>
#include "Note.h"
#include <sstream>
#define _(String) gettext (String)
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif //HAVE_CONFIG_H
#ifdef _WIN32
#include <windows.h>
#include <mmsystem.h>
#include <direct.h>
#endif //_WIN32
#include <glib.h>
#include <iomanip>

//Constructor
Gtk_Tuner::Gtk_Tuner(const Glib::RefPtr<Gtk::Builder> & refGlade, PaError error): m_refGlade(refGlade), m_menuitem_file(_("File")), m_menuitem_edit(_("Edit")), m_menuitem_help(_("Help")), m_menu_image_quit(Gtk::StockID("gtk-quit")), m_menu_image_about(Gtk::StockID("gtk-about")), m_menu_image_settings(Gtk::StockID("gtk-preferences")), m_old_SoundCard("")
{     
     std::string directory_icon; //Icon directory
     std::string directory_picture; //Pictures directory
     adjustement=Gtk::Adjustment::create(0,0,1000,1,10,10);
     Adjustment_Accurate=Gtk::Adjustment::create(0,0,1000,1,10,10);
     m_dlgProperties=nullptr;
     Data_Out=nullptr;
     m_Display=nullptr;
     Init_Search_Frequency();
     m_error=error;
     if (m_error!=paNoError)
     {
	  std::string message;
	  message=_("PortAudio can't initialize.\nThe application is going to stop");
	  message+=_("The error code is : ");
	  message+=m_error;
	  message+=_("\nThe meaning code is : ");
	  message+=Pa_GetErrorText(m_error);
	  Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
	  Dialog.set_title(_("Problem to initialize PortAudio"));
	  Dialog.run();
	  on_wndGuitarTuner_hide();
     }
#ifdef __linux__
     char *home; //Pointer to the char
     home=getenv("HOME");    //Get the home directory
     if (home!=NULL) //If home
     {
	  m_szparameters_xml=home;
	  m_szHome=home;  //Update the home variable
	  m_szparameters_xml+="/.guitar_tuner/guitar_tuner.xml";  //And the xml file directory
	  m_szHome+="/.guitar_tuner"; //And the guitar_tuner directory
     }
     else
     {
	  //Else error message
	  Glib::ustring message=Glib::ustring::compose(_("The variable named %1 doesn't exist\n"),"$HOME");
	  message+=_("Create this variable");
	  Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
	  Dialog.set_title(_("Environment variables problem"));
	  Dialog.run();
	  on_wndGuitarTuner_hide();   //Stop guitar tuner
     }
#elif defined (_WIN32)
     unsigned long result;
     char cHome[MAX_PATH];
//For  windaube, get the current guitar tuner directory
     result=GetCurrentDirectory(MAX_PATH,cHome);
     m_szHome=cHome;
     if (result)
     {
	  //and the directory of xml file
	  m_szparameters_xml=m_szHome;
	  m_szparameters_xml+="\\guitar_tuner.xml";
     }
     else
     {
	  //Else error
	  std::string message(_("Problem to find the software path\n")); 
	  message+=_("The software stopped");
	  Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
	  Dialog.set_title(_("Directory problem"));
	  Dialog.run();
	  on_wndGuitarTuner_hide();   //Stop application
     }
#endif
     //If config.h exist
#ifdef HAVE_CONFIG_H
     {
//The directory are in
	  m_szdirectory_share=GLADE_DIR;
	  directory_icon=( m_szdirectory_share+"/Guitare.svg");
     }
#else
     {
//Else the current directory
	  char directory_common[PATH_MAX];
	  getcwd(directory_common,PATH_MAX);
	  m_szdirectory_share=directory_common;
#ifdef __linux__
	  m_szdirectory_share+="/share";
	  directory_icon=( m_szdirectory_share+"/Guitare.svg");
#elif defined (_WIN32)
//Same thing for windaube
	  m_szdirectory_share+="\\share";
	  directory_icon=(m_szdirectory_share +"\\Guitare.png");
#endif
     }
#endif
     m_refGlade->get_widget_derived("dialog1",m_dlgProperties);
     m_dlgProperties->Set_Sound_Card(&soundcard);
//Max Level of signal
     param_display.Level_Max=0;
//Create menusx
     m_refActionGroup=Gio::SimpleActionGroup::create();
     m_refActionGroup->add_action("quit",sigc::mem_fun(*this,&Gtk_Tuner::on_Quit));
     m_refActionGroup->add_action("settings",sigc::mem_fun(*this,&Gtk_Tuner::on_menuProperties));
     m_refActionGroup->add_action("about",sigc::mem_fun(*this,&Gtk_Tuner::on_menuAbout));
     m_refActionGroup->add_action("analyse",sigc::mem_fun(*this,&Gtk_Tuner::onbuttonanalyse));
     m_refActionGroup->add_action("stop", sigc::mem_fun(*this,&Gtk_Tuner::onbuttonstop));
     m_refActionGroup->add_action("calibrate", sigc::mem_fun(*this,&Gtk_Tuner::onbuttoncalibrate));

     insert_action_group("gtk_tuner", m_refActionGroup);

     m_menuitem_file.set_submenu(m_menu_file);
     m_menuitem_edit.set_submenu(m_menu_settings);
     m_menuitem_help.set_submenu(m_menu_about);
     m_menu_image_quit.signal_activate().connect(sigc::mem_fun(*this,&Gtk_Tuner::on_Quit));
     m_menu_image_settings.signal_activate().connect(sigc::mem_fun(*this,&Gtk_Tuner::on_menuProperties));
     m_menu_image_about.signal_activate().connect(sigc::mem_fun(*this,&Gtk_Tuner::on_menuAbout));
     m_menu_file.append(m_menu_image_quit);
     m_menu_settings.append(m_menu_image_settings);
     m_menu_about.append(m_menu_image_about);
     m_menubar.append(m_menuitem_file);
     m_menubar.append(m_menuitem_edit);
     m_menubar.append(m_menuitem_help);
     Vbox1.pack_start(m_menubar, Gtk::PACK_SHRINK);
     m_refUIManager=Gtk::Builder::create();

Glib::ustring ui_info =
    "<!-- Generated with glade 3.18.3 -->"
    "<interface>"
    "  <requires lib='gtk+' version='3.4'/>"
    "  <object class='GtkToolbar' id='toolbar'>"
    "    <property name='visible'>True</property>"
    "    <property name='can_focus'>False</property>"
    "    <child>"
    "      <object class='GtkToolButton' id='toolbutton_analyse'>"
    "        <property name='visible'>True</property>"
    "        <property name='can_focus'>False</property>"
    "        <property name='tooltip_text' translatable='yes'>Analyse</property>"
    "        <property name='action_name'>gtk_tuner.analyse</property>"
    "        <property name='stock_id'>gtk-media-play</property>"
    "      </object>"
    "      <packing>"
    "        <property name='expand'>False</property>"
    "        <property name='homogeneous'>True</property>"
    "      </packing>"
    "    </child>"
    "    <child>"
    "      <object class='GtkToolButton' id='toolbutton_stop'>"
    "        <property name='visible'>True</property>"
    "        <property name='can_focus'>False</property>"
    "        <property name='tooltip_text' translatable='yes'>Stop</property>"
    "        <property name='action_name'>gtk_tuner.stop</property>"
    "        <property name='stock_id'>gtk-media-stop</property>"
    "      </object>"
    "      <packing>"
    "        <property name='expand'>False</property>"
    "        <property name='homogeneous'>True</property>"
    "      </packing>"
    "    </child>"
    "    <child>"
    "      <object class='GtkToolButton' id='toolbutton_preferences'>"
    "        <property name='visible'>True</property>"
    "        <property name='can_focus'>False</property>"
    "        <property name='tooltip_text' translatable='yes'>Settings</property>"
    "        <property name='action_name'>gtk_tuner.settings</property>"
    "        <property name='stock_id'>gtk-properties</property>"
    "      </object>"
    "      <packing>"
    "        <property name='expand'>False</property>"
    "        <property name='homogeneous'>True</property>"
    "      </packing>"
    "    </child>"
    "    <child>"
    "      <object class='GtkToolButton' id='toolbutton_calibrate'>"
    "        <property name='visible'>True</property>"
    "        <property name='can_focus'>False</property>"
    "        <property name='tooltip_text' translatable='yes'>Calibrate</property>"
    "        <property name='action_name'>gtk_tuner.calibrate</property>"
    "      </object>"
    "      <packing>"
    "        <property name='expand'>False</property>"
    "        <property name='homogeneous'>True</property>"
    "      </packing>"
    "    </child>"     
    "  </object>"
    "</interface>";

     std::string message(_("Problem to create menus"));
     try
     {
	  m_refUIManager->add_from_string(ui_info);
     }
     catch(const Glib::Error &ex)
     {
#ifdef GLIBMM_EXCEPTIONS_ENABLED
	  Gtk::MessageDialog Dialog(ex.what(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
	  Dialog.set_title(message);
	  Dialog.run();   // Message error
#else
	  std::auto_ptr<Glib::Error> ex;
	  m_refUIManager->add_from_string(ui_info,ex);
	  if (ex.get())
	  {
	       std::cerr<<message<<ex->what();
	  }
#endif //GLIBMM_EXCEPTIONS_ENABLED
     }
     Gtk::Toolbar *pToolbar=nullptr;
     m_refUIManager->get_widget("toolbar", pToolbar);
  if (pToolbar)
  {
       Vbox1.pack_start(*pToolbar,Gtk::PACK_SHRINK);
       m_refUIManager->get_widget("toolbutton_preferences", m_tbsettings);
       m_refUIManager->get_widget("toolbutton_analyse",m_tbanalyse);
       m_refUIManager->get_widget("toolbutton_calibrate", m_tbcalibrate);
  }
  //create the calibrate picture for le toolbutton calibrate
  if (m_tbcalibrate)
  {
       m_pic_calibrate.set(m_szdirectory_share+"/army_target.png");
       m_tbcalibrate->set_icon_widget(m_pic_calibrate);
  }   
     bDisplaySpectral=false;    //No Spectral window
     set_resizable(false);
//Create the context for sound card, chord, frequency etc...
     for(int i=0;i<5;i++)
     {
	  std::string context;
	  std::ostringstream o_stream;
	  context="ctx";
	  o_stream<<i;
	  context+=o_stream.str();
//TODO     stsInfo[i].set_has_resize_grip(false);
	  Hbox6.pack_start(stsInfo[i]);
	  ctx[i]= stsInfo[i].get_context_id(context);
     }
     Vbox3.pack_start(Hbox1);
     Vbox3.pack_start(Hbox2);
     Vbox3.pack_start(Hbox3);
     Vbox3.pack_start(Hbox4);
     Vbox3.pack_start(Hbox5);
#ifdef __linux__
     directory_picture=(m_szdirectory_share+"/repere.png");
#elif defined _WIN32
     directory_picture=(m_szdirectory_share+"\\repere.png");
#endif 
     Hbox3.pack_start(hsLevel);
     hsLevel.set_draw_value(false);
     hsLevel.set_adjustment(adjustement);
     Hbox4.pack_start(Eventbox[6],Gtk::PACK_EXPAND_WIDGET);
     Eventbox[6].add(Pictures[6]);
     Pictures[6].set(directory_picture);
     Hbox5.pack_start(hsAccurate, Gtk::PACK_EXPAND_WIDGET);
     hsAccurate.set_draw_value(false);
     hsAccurate.set_adjustment(Adjustment_Accurate);
     for (int i=0; i<6;i++)
     {
	  Eventbox[i].add(lblNotes[i]);
	  Pictures[i].set(directory_picture);
     } 
//Connect the hide signal
     signal_hide().connect(sigc::mem_fun(*this,&Gtk_Tuner::on_wndGuitarTuner_hide)),
//Put the context in the main window
	  Vbox1.pack_start(Hbox6,Gtk::PACK_SHRINK);
     add(Vbox1);
//Update the icon application
     set_default_icon_from_file(directory_icon);
     set_title(_("Guitar Tuner"));
//Main window always on top
     set_keep_above(true);
     set_position(Gtk::WindowPosition::WIN_POS_CENTER);
     show_all_children();
}

void Gtk_Tuner::Init_Search_Frequency(void)
{
     m_current_frequency_detected=0;
     m_current_frequency_displayed=0;
     m_occ_frequency=0;
}

void Gtk_Tuner::Display_Drawing_Area(void)
{
     if (m_Display!=NULL)
     {
	  delete m_Display;
     }
     m_Display=new Gtk::DrawingArea();
     Vbox1.pack_start(*m_Display);
     Vbox1.reorder_child(Hbox6,-1);
     set_size_request(800,600);
}

void Gtk_Tuner::Display_Tuner(void)
{
     Display_Drawing_Area();
     m_Display->signal_draw().connect(sigc::mem_fun(*this,&Gtk_Tuner::on_DrawingTuner));
     show_all_children();
}

//Round Display
void Gtk_Tuner::Display_Round(void)
{
     Display_Drawing_Area();
     m_Display->signal_draw().connect(sigc::mem_fun(*this,&Gtk_Tuner::on_DrawingRound));
     show_all_children();
}

//Standard display
void Gtk_Tuner::Display_Standard(void)
{
     int j=0;
     set_size_request(1030,460);
     while (!Hbox1.get_children().empty())
     {
	  Hbox1.remove(Eventbox[j]);
	  Hbox2.remove(Pictures[j]);
	  j++;
     }
     for(int i=0;i<Notes_Chord.GetNumberNote();i++)
     {
	  Hbox1.pack_start(Eventbox[i]);
	  
	  Hbox2.pack_start(Pictures[i]);
     }
     //Get the style of the EventBox 
//     style=Eventbox[0].get_style_context();
     Vbox1.pack_start(Vbox3);
     Vbox1.reorder_child(Hbox6,-1);
     show_all_children();
}

//Stop the analysis
void Gtk_Tuner::onbuttonstop()
{
     if (pthread.connected()) //If thread exist
     {
	  pthread.disconnect();
	  m_tbsettings->set_sensitive();
	  m_menu_image_settings.set_sensitive();
	  m_tbanalyse->set_sensitive();
	  bDisplaySpectral=false;    //Not spectral window
     }
     soundcard.StopRecord();
        //Update the level
     param_display.Level_Max=0;
     Data_Out=nullptr;
     ColorDisplay(1,param_display.Color,false);
     hsAccurate.set_value(0);
     hsLevel.set_value(0);
}

//When you press the properties button
void Gtk_Tuner::onproperties()
{
     if (!bSoundCard) //If no sound card
     {
	  on_wndGuitarTuner_hide();   //Stop guitar tuner
     }
     else
     {
	  if (!bChord)   //Same thing with no Chord xml file
	  {
	       std::string message(_("Problem to find the chords file\n"));
	       message+=_("The software stopped");
	       Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
	       Dialog.set_title(_("Problem to open the chords file"));
	       Dialog.run();
	       on_wndGuitarTuner_hide();
	  }
	  else
	  {
	       int answer;    //For the answer of the properties dialog box
	       if (param_xml.bFileOK==false)    //If there no parameters file
	       {
                    param_xml.nNumberSample=16384;   //Update with default values
                    param_xml.bDispFreq=false;
                    param_xml.nAttenuation=5;
                    param_xml.nNumber_sound_card=0;
                    param_xml.nFreqSampling=24000;
                    param_xml.nNumberChord=0;
		    param_xml.nDisplay_Interface=2;
		    param_xml.bDispMark=false;
		    param_xml.szNote=_("C");
		    param_xml.sOctave=0;
		    param_xml.dFrequency=0;
		    param_xml.sTunerMode=0;
		    param_xml.nCalibratePeriod=10;
		    param_xml.Persistence_Note_Delay=20;
	       }
	       if (m_dlgProperties!=NULL)
	       {
		    m_dlgProperties->SetPathShare(m_szdirectory_share);
		    m_dlgProperties->SetPathParam_XML(m_szparameters_xml);    //and file directory
		    m_dlgProperties->SetParameters(param_xml);  //Update the parameters for the dialog box
		    answer=m_dlgProperties->run();  //Display dialog box
		    if ((answer!=3) && (!param_xml.bFileOK))   //If press cancel and no param files
		    {
			 m_dlgProperties->hide();
			 std::string message(_("The settings are unknown\n"));
			 message+=_("The analyse is impossible");
			 Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
			 Dialog.set_title(_("Unknown settings"));
			 Dialog.run();
			 m_tbanalyse->set_sensitive(false);
		    }
		    else
			 UpdateControls();
	       }
	  }
     }
}

void Gtk_Tuner::onbuttoncalibrate()
{
     soundcard.Calibrate(param_xml, Notes_Chord);
     m_calibration=soundcard.Get_Calibration();
}

//When you press the analyse button
void Gtk_Tuner::onbuttonanalyse()
{
//First image for the index
     alloc=Pictures[0].get_allocation();
     nPositionFirstIndex=Pictures[0].get_allocation().get_width()/2;
     nIntervalInterIndex=Pictures[1].get_allocation().get_x()-Pictures[0].get_allocation().get_x();
     Init_Search_Frequency();
     param_display.Frequency_Peak=0;
     StartThread();
     soundcard.StartRecord(param_xml, Notes_Chord, false);
}

int Gtk_Tuner::AnalyseSignal(void)
{
     if (soundcard.IsRecordStarted()==1 && soundcard.IsThreadStarted())
     {
	  m_tbanalyse->set_sensitive(false);
	  m_menu_image_settings.set_sensitive(false);
	  m_tbsettings->set_sensitive(false);
     }
     if (Data_Out==nullptr)
     {
	  Data_Out=soundcard.Get_Data_Out();
     }

     if (soundcard.Is_Data_Freshed())
     {
	  bDisplaySpectral=true;
	  //Update the frequency
	  UpdateFrequency();
	  //Update the standard display
	  if (param_xml.nDisplay_Interface==0)
	       UpdateSlider();
	  else if ((param_xml.nDisplay_Interface==1) || (param_xml.nDisplay_Interface==2))
	  {
	       //Or the round display
	       Glib::RefPtr<Gdk::Window> win = m_Display->get_window();
	       if (win)
	       {
		    Gdk::Rectangle r(0, 0, m_Display->get_allocation().get_width(),
				     m_Display->get_allocation().get_height());
		    win->invalidate_rect(r, false);
	       }
	  } 
     }     
     return true;
}

//Start the thread
void Gtk_Tuner::StartThread(void)
{
     if (!pthread.connected())   //If no thread
     {
	  //Create the thread
	  pthread=Glib::signal_idle().connect( sigc::mem_fun(*this, &Gtk_Tuner::AnalyseSignal),Glib::PRIORITY_HIGH_IDLE + 20  /*Glib::PRIORITY_DEFAULT_IDLE*/);
     }
}


//Destructor
Gtk_Tuner::~Gtk_Tuner()
{
     onbuttonstop();
     Pa_Terminate();
     if (m_Display!=NULL)
     {
	  delete m_Display;
     }
}

//When the chord changed
int Gtk_Tuner::Chord_changed()
{
     int ret;    //error return
     CNote Note;
     std::string szName_Note;
     std::ostringstream oss;
     char *ChordName;    //Pointer for the chord name
     xmlDocPtr xmlChord_doc = NULL; //Pointer for the xml document
     xmlXPathContextPtr xmlparameters_context = NULL;    //Pointer for the context
     xmlXPathObjectPtr xmlobject_chord,xmlobject_Chord_Name, xmlobject_Note_Name;  //Objects pointer
     xmlNodePtr node;    //Node Pointer 
     ret=1;
     Notes_Chord.ClearNotes();
     switch (param_xml.sTunerMode)
     {
     case 1:	 
	  szName_Note=param_xml.szNote;
	  oss<<param_xml.sOctave;
	
	  szName_Note+=oss.str();
	  Note.SetNoteName(szName_Note);
	  if (Note.GetValidity()==true)
	  {
	       Notes_Chord.AddNote(Note);
	       Notes_Chord.SetChord(szName_Note);
	  }
	  else
	       ret=-1;
	  break;
     case 2:
	  Note.SetFrequency(param_xml.dFrequency);
	  Notes_Chord.AddNote(Note);
	  oss<<param_xml.dFrequency;
	  szName_Note=oss.str()+" Hz";
	  Notes_Chord.SetChord(szName_Note);
	  break;
     default:
	  //Get the doc pointer
	  xmlChord_doc = xmlParseFile (m_szdirectory_chord_xml.c_str());
	  if (xmlChord_doc)
	  {
	       //Path xml doc init
	       xmlXPathInit ();
	       //Get the context
	       xmlparameters_context = xmlXPathNewContext (xmlChord_doc);
	       if (xmlparameters_context)
	       {
		    //Get the path for the name 'accord'
		    std::string path_temp,path_chord("/accords/accord"),path_name_chord;
		    std::ostringstream number;
		    number<<param_xml.nNumberChord+1;
		    path_chord+=number.str();
		    path_temp=path_chord;
		    path_name_chord=path_chord + "/nom/text()";   //Get the path of accord 
		    path_chord+="/text()"; // and the data node
		    xmlobject_chord = xmlXPathEval ((const xmlChar*)path_chord.c_str(), xmlparameters_context);
		    xmlobject_Chord_Name = xmlXPathEval ((const xmlChar*)path_name_chord.c_str(), xmlparameters_context);
		    if (xmlobject_Chord_Name->nodesetval->nodeNr)
		    {
			 //Get the name of the chord
			 node=xmlobject_Chord_Name->nodesetval->nodeTab[0];
			 ChordName=(char*)node->content;
			 //Update the CChord Object
			 Notes_Chord.SetChord(ChordName);
			 //For all the chord notes
			 for (int Index_Note=1;Index_Note<xmlobject_chord->nodesetval->nodeNr-1;Index_Note++)
			 {
			      std::ostringstream Number_note;   //Note number
			      std::string path_notes(path_temp), path_temp_note;  //The notes path
			     
			    
			      path_notes+="/note";
			      Number_note<<Index_Note;
			      path_notes+=Number_note.str();
			      path_temp_note=path_notes;
			      path_notes+="/Index_Note/text()";  //Path of the object
			      xmlobject_Note_Name = xmlXPathEval ((const xmlChar*)path_notes.c_str(), xmlparameters_context);
			      if (xmlobject_Note_Name->nodesetval->nodeNr)
			      {
				   //Get name note
				  
				   int nIndex_Note_Chord;
				   node=xmlobject_Note_Name->nodesetval->nodeTab[0];
				   std::istringstream iss((const char*)node->content);
				   iss>>nIndex_Note_Chord;
				   szName_Note=Name_Note[nIndex_Note_Chord];
				   path_notes=path_temp_note+"/Hauteur/text()";
				   xmlobject_Note_Name = xmlXPathEval ((const xmlChar*)path_notes.c_str(), xmlparameters_context);
				   if (xmlobject_Note_Name->nodesetval->nodeNr)
				   {
					std::string szLevel;
					node=xmlobject_Note_Name->nodesetval->nodeTab[0];	
					szLevel= (char*)node->content;		       
					szName_Note+=szLevel;
					Note.SetNoteName(szName_Note);
					if (Note.GetValidity())
					     Notes_Chord.AddNote(Note);
					else
					     ret=-1;					    
				   }
				   else
					ret=-1;
			      }
			      else
				   ret=-1;			    
			 }
		
			 //Free all the memory
			 xmlXPathFreeObject(xmlobject_chord);
			 xmlXPathFreeObject(xmlobject_Chord_Name);
			 xmlXPathFreeObject(xmlobject_Note_Name);
			 xmlXPathFreeContext(xmlparameters_context);               
		    }
		    else
			 ret=-1;
		    xmlFreeDoc(xmlChord_doc);  //Free the doc pointer
	       }
	       else
	       {
		    std::string message(_("Problem to create a xml context"));
		    Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
		    Dialog.set_title("XML Problem");
		    Dialog.run();
		    ret=-1;
	       }
	  }
	  else
	  {
	       Glib::ustring message=Glib::ustring::compose(_("Problem to load the %1 chords file "),m_szdirectory_chord_xml);
	       Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
	       Dialog.set_title(_("Problem chords file"));
	       Dialog.run();
	       ret=-1;
	  }
   
     }
     //Update the labels
     for (int IndexLabel=0;IndexLabel<Notes_Chord.GetNumberNote();IndexLabel++)
     {
	 
	  if (param_xml.sTunerMode<2)
	       lblNotes[IndexLabel].set_text(Notes_Chord.GetNote(IndexLabel)->GetNoteName());
	  else
	       lblNotes[IndexLabel].set_text(Notes_Chord.GetNameChord());
     }
     if (param_xml.nDisplay_Interface==1)
	  show_all_children();
	
     return ret;
}

//When the programm stop
void Gtk_Tuner::on_wndGuitarTuner_hide()
{
     onbuttonstop();
     hide();
}

bool Gtk_Tuner::on_DrawingTuner(const Cairo::RefPtr<Cairo::Context>&cr)
{
     Glib::RefPtr<Gdk::Window> win = m_Display->get_window();
     double half_gap, Note_angle;
     int direction;
     if (win)
     {
//Get the size of the window
	  Gtk::Allocation allocation = m_Display->get_allocation();
	  const int width = allocation.get_width();
	  const int height = allocation.get_height();
	  std::string car;
	  CNote Note_Peak(param_display.Frequency_Peak);
	  double tuning_frequency;
	  Pango::FontDescription font;
	  //The zone in white
	  cr->save();
	  cr->set_source_rgba(1, 1, 1, 0.9);
	  cr->paint();
	  cr->restore();
	  cr->save();
	  cr->set_source_rgba(0, 0, 0, 0.9);
	  // Write some characters
	  cr->move_to(width/2, height/20);
	  font.set_family("Liberation Serif" );
	  font.set_absolute_size(20*Pango::SCALE);
	  font.set_weight(Pango::WEIGHT_ULTRAHEAVY);
	  car="0";
	  auto layout = create_pango_layout(car);
	  layout->set_font_description(font);
	  layout->show_in_cairo_context(cr);
	  cr->move_to(width/2+(width/2)*cos(G_PI_2+4*2*G_PI_2/(3*11)),height- height*sin(G_PI_2+4*2*G_PI_2/(3*11)));
	  car="-20";
	  layout = create_pango_layout(car);
	  layout->set_font_description(font);
	  layout->show_in_cairo_context(cr);
	  cr->move_to(width/2-(width/2)*cos(G_PI_2+4*2*G_PI_2/(3*14)),height- height*sin(G_PI_2+4*2*G_PI_2/(3*11)));
	  car="+20";
	  layout = create_pango_layout(car);
	  layout->set_font_description(font);
	  layout->show_in_cairo_context(cr);
	  cr->save();
	  cr->stroke();
	  //Write the indexes of the tuning scale
	  for (size_t i=0;i<12;i++)
	  {
	       double xc, yc, radius;
	       if (i==0 || i==3)
	       {
		    radius=5;
	       }
	       else if (i%2==0)
	       {
		    radius=3;
	       }
	       else
	       {
		    radius=1;
	       }
	       cr->save();
	       xc=(width/2)*cos(G_PI_2+i*2*G_PI_2/(3*11));
	       yc=sin(G_PI_2+i*2*G_PI_2/(3*11));

	       cr->arc(width/2+xc,height+height/10- height*yc, radius, 0,2*M_PI);
	       cr->fill();
	       cr->stroke();
	       cr->arc(width/2-xc,height+height/10- height*yc, radius, 0,2*M_PI);
	       cr->fill();
	       cr->stroke();
	  }
//Watch the slider
	  if ((soundcard.IsThreadStarted() == 1) && (param_display.Frequency_Peak!=0))
	  {
	       half_gap=(Note_Peak.GetFrequencyAbove()-Note_Peak.GetFrequencyBelow())/2;
	       if (param_display.Frequency_Peak>half_gap+Note_Peak.GetFrequencyBelow())
	       {
		    tuning_frequency=Note_Peak.GetFrequencyAbove();
	       }
	       else
	       {
		    tuning_frequency=Note_Peak.GetFrequencyBelow();
	       }
	       if (param_display.Frequency_Peak>tuning_frequency)
	       {
		    direction=1;
	       }
	       else
	       {
		    direction=-1;
	       }
	       Note_angle=G_PI_2*(abs(tuning_frequency-param_display.Frequency_Peak))/half_gap;
	       cr->move_to(width / 12, height / 12);
	       car = Note_Peak.GetNoteName();
	       font.set_absolute_size(50*Pango::SCALE);
	       layout = create_pango_layout(car);
	       layout->set_font_description(font);
	       layout->show_in_cairo_context(cr);
	       cr->save();
	       cr->stroke();
	       if (Note_angle<G_PI/11) 
	       {
		    cr->set_source_rgba(0, 1, 0, 0.9);
		    cr->arc(width/2,height/30, 10,0,2*M_PI);
		    cr->fill();
	       }
	       if (Note_angle>G_PI/(3*11))
	       {
		    cr->set_source_rgba(1, 0, 0, 0.9);
		    cr->arc(width/2-direction*(width/2)*cos(G_PI_2+4*2*G_PI_2/(3*14)),height/30, 10,0,2*M_PI);
		    cr->fill();
	       }
	       cr->save();
	       cr->stroke();
          }
	  else
	  {
	       Note_angle=0;
	  }
	  cr->set_source_rgba(0,0,0, 0.9);
	  cr->move_to(width/2, height);
	  cr->line_to(width/2+direction*sin(Note_angle)*width/2,height/10+sin(Note_angle)*sin(Note_angle)*height*9/10);
	  cr->save();
	  cr->stroke();
	  
            // Faire une couleur selon la précision de la note
     }
     return true;
}

//When update round display
bool Gtk_Tuner::on_DrawingRound(const Cairo::RefPtr<Cairo::Context>& cr)
{
     int fallback,position_y_level;
     Gdk::Color Black;
     const int line_size=20;
     double xc,yc,xline,yline;
     int wp,hp;
     double red=0,yellow=0,  dMi_numberNote;
     Black.set_rgb(0,0,0);
//Get the window pointer
     Glib::RefPtr<Gdk::Window> win = m_Display->get_window();	  
     if (win)
     {
//Get the size of the window
	  Gtk::Allocation allocation = m_Display->get_allocation();
	  const int width = allocation.get_width();
	  const int height = allocation.get_height();
	  //The zone in white
	  cr->save();
	  cr->set_source_rgba(1, 1, 1, 0.9);
	  cr->paint();
	  cr->restore();
	  cr->stroke();
	  int less=MIN(width,height);
	  cr->save();
	  cr->arc(width/2,height/2,less/3,0,2.0*M_PI);
	  cr->set_source_rgba(1,1,1,0.9);
	  cr->fill_preserve();
	  cr->restore();
	  cr->stroke();
	  dMi_numberNote=((double)Notes_Chord.GetNumberNote())/2;
	  cr->set_source_rgba(0, 0, 0, 0.9);
//Draw the notes names, color etc ...
	  for (int i=0; i<Notes_Chord.GetNumberNote();i++)
	  {
	       std::string sznote;
	       Pango::FontDescription font;
	       font.set_family("sans bold 20");
	       font.set_weight(Pango::WEIGHT_BOLD);
	       if (param_xml.sTunerMode<2)
		    sznote=Notes_Chord.GetNote(i)->GetNoteName();
	       else
		    sznote=Notes_Chord.GetNameChord();
	       auto layout = create_pango_layout(sznote);
	       layout->set_font_description(font);
	       layout->get_pixel_size(wp,hp);
	       fallback=hp/2;
	       xc=width/2+less/3*cos(G_PI/2-i*G_PI/dMi_numberNote);
	       yc=height/2-less/3*sin(G_PI/2-i*G_PI/dMi_numberNote);
	       xline=xc;
	       yline=yc;
	       cr->save();
	       cr->move_to(xline,yline);
	       cr->line_to(xline-less/line_size*cos(G_PI/2-i*G_PI/dMi_numberNote),yline+less/line_size*sin(G_PI/2-i*G_PI/dMi_numberNote));
	       if (xc<width/2)
		    xc-=wp*1.5;
	       else if (xc>width/2)
		    xc+=wp/2;
	       if ((yc<height/2) && (xc!=width/2))
		    yc-=fallback;
	       if (xc==width/2)
	       {
		    xc-=wp/2;
		    if (yc<height/2)
			 yc-=fallback*3;
		    else
			 yc+=fallback;
	       }
	       if (i==param_display.Index_Note_Near)
	       {
		    cr->set_source_rgb(param_display.Color.get_red(),param_display.Color.get_green(),param_display.Color.get_blue());
		    if (fabs(param_display.Frequency_Peak-Notes_Chord.GetNote(param_display.Index_Note_Near)->GetFrequency())<0.05)
		    {
			 const int shift=line_size/2;
			 cr->rectangle(xc-shift,yc-shift,wp+2*shift,hp+2*shift);
		    }
	       }
	       else
	       {
		    cr->set_source_rgb(0,0,0);
	       }
	       cr->rectangle(xc,yc,wp,hp);
	       layout->show_in_cairo_context(cr);
	       cr->stroke();
	  }
//Display the level
	  if (param_display.Level_Max!=0)
	  {
	       position_y_level=height/4;
	       for (double i=1E7;((i<param_display.Level_Max) && (position_y_level>0));i*=1.2)
	       {
		    if (i>1E8)
		    {
			 yellow+=0.15;
			 cr->set_source_rgb(1,1-yellow,0);
		    }
		    else
		    {
			 cr->set_source_rgb(red,1,0);
			 red+=0.15;
		    }
		    cr->move_to(width-width/16,position_y_level);
		    cr->line_to(width-width/8,position_y_level);
		    position_y_level-=5;
		    cr->stroke();
	       }
	  }
//Draw the accurate line, note line and color
	  if (param_display.Frequency_Peak!=0)
	  {
	       double Angle_Line,Angle_delta;
	       Angle_delta=((param_display.Frequency_Peak-param_display.Note_Below)/(param_display.Note_Sup-param_display.Note_Below))*G_PI/dMi_numberNote;
	       Angle_Line=G_PI/2-(param_display.Index_Note_Below*G_PI/dMi_numberNote+Angle_delta);
	       cr->set_source_rgb(param_display.Color.get_red(),param_display.Color.get_green(),param_display.Color.get_blue());
	       cr->move_to(width/2,height/2);
	       cr->line_to(width/2+cos(Angle_Line)*(less/3-line_size*1.5),height/2-sin(Angle_Line)*(less/3-line_size*1.5));
	       cr->move_to(width/2,height/2);
	       if (param_display.precision_ratio>1.01)
	       {
		    cr->line_to(width/2+less/3,height/2);
	       }
	       else if (param_display.precision_ratio<0.99)
		    cr->line_to(width/2-less/3,height/2);
	       else if ((param_display.precision_ratio>1) && (param_display.precision_ratio<1.01))
		    cr->line_to(width/2+less/3*(param_display.precision_ratio-1)*100,height/2);
	       else if ((param_display.precision_ratio<1) && (param_display.precision_ratio>0.99))
		    cr->line_to(width/2-less/3*(1-param_display.precision_ratio)*100,height/2);
	       cr->stroke();
	  }
     }
     return true;
}

//When the sound card change
int Gtk_Tuner::SoundCard_changed(void)
{
     int err;
     //Get the sound card characteristic
     err=soundcard.SetParameters(param_xml);
     return err;
}

void Gtk_Tuner::Get_Frequency_To_Displayed(double frequency)
{
     if (frequency!=0)
     {
	  if (frequency==m_current_frequency_detected)
	  {
	       m_occ_frequency++;
	  }
	  else
	  {
	       m_occ_frequency=0;
	       m_current_frequency_detected=frequency;
	  }
	  if (m_occ_frequency > param_xml.Persistence_Note_Delay)
	  {
	       m_current_frequency_displayed = frequency;
	  }
     }
}

//Update the parameters of the detected frequency
void Gtk_Tuner::UpdateFrequency(void)
{
     Glib::ustring glfreq;
     Get_Frequency_To_Displayed(SearchPeak());
     param_display.Frequency_Peak=m_current_frequency_displayed;	//Get the frequency
     glfreq=Glib::ustring::format(std::setprecision(3),std::fixed,param_display.Frequency_Peak);
     glfreq=_("Frequency:")+glfreq;
     stsInfo[4].push(glfreq,ctx[4]);
     param_display.Index_Note_Below=Notes_Chord.GetNoteBel(param_display.Frequency_Peak); //Get the note index below
     param_display.Index_Note_Sup=Notes_Chord.GetNoteAbo(param_display.Frequency_Peak);	//Get the note index Above
     if (param_display.Index_Note_Below>-1)	//If the note below is in the chord
     {
	  param_display.Note_Below=Notes_Chord.GetNote(param_display.Index_Note_Below)->GetFrequency(); //Get the note frequency below
     }
     else	
     {
	  param_display.Note_Below=Notes_Chord.GetNote(0)->GetFrequency()/2;	//Note below is the minimum note of the chord / 2
     }
     if (param_display.Index_Note_Sup<Notes_Chord.GetNumberNote())	//If the note Above is in the chord
     {
	  param_display.Note_Sup=Notes_Chord.GetNote(param_display.Index_Note_Sup)->GetFrequency(); //Get the note frequency above
     }
     else	
     {
	  //Note above is calculated 
	  param_display.Note_Sup=Notes_Chord.GetNote(Notes_Chord.GetNumberNote()-1)->GetFrequency()+Notes_Chord.GetNote(0)->GetFrequency()-10;
     }
     if (abs(int(param_display.Note_Sup-param_display.Frequency_Peak))<abs(int(param_display.Note_Below-param_display.Frequency_Peak)))
     {
	  //The slider is up to right
	  param_display.Index_Note_Near=param_display.Index_Note_Sup;
     }
     else
     {
	  //Else up to left
	  param_display.Index_Note_Near=param_display.Index_Note_Below;
     }
     if ((param_display.Index_Note_Near>-1) && (param_display.Index_Note_Near<Notes_Chord.GetNumberNote()))
     {
//Update the label color
	  if ((param_display.Frequency_Peak/Notes_Chord.GetNote(param_display.Index_Note_Near)->GetFrequency()>=0.995) && (param_display.Frequency_Peak/Notes_Chord.GetNote(param_display.Index_Note_Near)->GetFrequency()<=1.005))
	  {
	       //// Green when the delta note is  > 0.995 et < 1.005
	       param_display.Color.set_rgba(0,65535,0);
	       param_display.bcolor=true;
	  }
	  else if ((param_display.Frequency_Peak/Notes_Chord.GetNote(param_display.Index_Note_Near)->GetFrequency()>=0.99) && (param_display.Frequency_Peak/Notes_Chord.GetNote(param_display.Index_Note_Near)->GetFrequency()<=1.01))
	  {
	       //Yellow beetween 0.99 et 1.01
	       param_display.Color.set_rgba(65535,65535,0);
	       param_display.bcolor=true;
	  }
	  else
	  {
	       //No color else
	       param_display.Color.set_rgba(0,0,0);
	       param_display.bcolor=false;
	  }
	  param_display.precision_ratio=param_display.Frequency_Peak/Notes_Chord.GetNote(param_display.Index_Note_Near)->GetFrequency();
     }
     else
     {
	  if (param_display.Index_Note_Near==-1)
	       param_display.precision_ratio=0;
	  else if (param_display.Index_Note_Near==Notes_Chord.GetNumberNote())
	  {
	       param_display.precision_ratio=param_display.Frequency_Peak/param_display.Note_Sup;
	  }
	  param_display.Color.set_rgba(0,0,0);
	  param_display.bcolor=false;
     }
}

//Update the Slider position
void Gtk_Tuner::UpdateSlider(void)
{
     int LevelBottom,LevelUpper;
     double factor;
     int SliderPosition;
     if (param_display.Index_Note_Below>-1)	//If the below index is in the chord
     {
	  LevelBottom=nPositionFirstIndex+param_display.Index_Note_Below*nIntervalInterIndex;	//Get the index position
     }
     else
	  LevelBottom=0;
     if (param_display.Index_Note_Sup<Notes_Chord.GetNumberNote())	//If the index upper is in the chord
     {
	  LevelUpper=nPositionFirstIndex+param_display.Index_Note_Sup*nIntervalInterIndex;	//Get the index position
     }
     else
     {
	  //Else the index position is the upper position of the slider
	  LevelUpper=hsLevel.get_adjustment()->get_upper();
     }
     factor=(param_display.Frequency_Peak-param_display.Note_Below)/(param_display.Note_Sup-param_display.Note_Below);
     SliderPosition=((LevelUpper-LevelBottom)*factor+LevelBottom)*(hsLevel.get_adjustment()->get_upper()/(nPositionFirstIndex*2+nIntervalInterIndex*5));
     //Update the position of the accurate slider
     UpdateSliderAccurate(param_display.precision_ratio);
     hsLevel.get_adjustment()->set_value(SliderPosition);	//Update the position of the slider
     if ((param_display.Index_Note_Near>-1 ) && (param_display.Index_Note_Near<Notes_Chord.GetNumberNote()))
     {
	  ColorDisplay(param_display.Index_Note_Near,param_display.Color,param_display.bcolor);
     }
     else if (param_display.Index_Note_Near<0)
     {
	  ColorDisplay(0, param_display.Color, false);
     }
}

//Search the frequency peak
double Gtk_Tuner::SearchPeak(void)
{
     double peakTemp=0,stopmax,stopmin,peak;
     long int i,IndexPeak=0,IndexStopMax;
     bool bNoteFound=false;
     double level;
     double freq=0;
     param_display.Level_Max=0;
     //Get the stop note of the Slider
     stopmax=Notes_Chord.GetNote(Notes_Chord.GetNumberNote()-1)->GetFrequency()+Notes_Chord.GetNote(0)->GetFrequency()-10;
     //Index in the samples 
     IndexStopMax=(long int)((stopmax*param_xml.nNumberSample)/param_xml.nFreqSampling);
     stopmin=Notes_Chord.GetNote(0)->GetFrequency()/2;
     i=(long int)((stopmin*param_xml.nNumberSample)/param_xml.nFreqSampling);
     //Get the attenuation value
     level=soundcard.Get_Level()*((double)param_xml.nAttenuation/10+1);
     while ((!bNoteFound) && (i<IndexStopMax))
     {
	  peakTemp=sqrt(pow(Data_Out->RealOut[i],2)+pow(Data_Out->ImagOut[i],2));
	  if (m_calibration!=nullptr)
	  {
	       peakTemp-=m_calibration[i];
	  }
	  if (peakTemp>level)
	  {
	       std::cout<<"Sup Level : "<<(double)(i*param_xml.nFreqSampling)/param_xml.nNumberSample<<std::endl;
	       if ((bNoteFound=ValidatePeak(i))==true)	//If the peak is valid
	       {
		    std::cout<<"Validé : "<<(double)(i*param_xml.nFreqSampling)/param_xml.nNumberSample<<std::endl;
		    //Update the value of the peak and is sample index
		    peak=peakTemp;
		    IndexPeak=i;
		    //Return the frequency of the peak
		    freq=(double)(IndexPeak*param_xml.nFreqSampling)/param_xml.nNumberSample;
	       }
	  }
	  i++;
     }
     return freq;
}

//Search the validity of the peak
bool Gtk_Tuner::ValidatePeak(int IndexCentralFrequency)
{
     bool ret=false;
     int j=1;
     double peak;
     if (IndexCentralFrequency>0)	//If sample index > 0
     {
	  ret=true;
	  peak=sqrt(pow(Data_Out->RealOut[IndexCentralFrequency],2)+pow(Data_Out->ImagOut[IndexCentralFrequency],2));
	  //Search around samples index -20 +20, there are no peak bigger
	  while ((ret==true) && (j<20))
	  {
	       double peak_decrement, peak_increment;
	       peak_decrement=sqrt(pow(Data_Out->RealOut[IndexCentralFrequency-j],2)+pow(Data_Out->ImagOut[IndexCentralFrequency-j],2));
	       peak_increment=sqrt(pow(Data_Out->RealOut[IndexCentralFrequency+j],2)+pow(Data_Out->ImagOut[IndexCentralFrequency+j],2));
	       if (m_calibration!=nullptr)
	       {
		    peak_decrement-=m_calibration[IndexCentralFrequency-j];
		    peak_increment-=m_calibration[IndexCentralFrequency+j];
		    peak-=m_calibration[IndexCentralFrequency];
	       }
	       if ((peak_decrement<peak) && (peak_increment<peak))
		    ret=true;	// then true
	       else ret=false;
	       j++;
	  }
     }
     return ret;
}

//Change color of the widget background (EventBox)
void Gtk_Tuner::Change_Color_Widget(Glib::RefPtr<Gtk::StyleContext> style, Gdk::RGBA Color)
{
     const Glib::ustring szcolor=Color.to_string();
     std::string style_sheet=".color_bg {background: "+szcolor+"; color: #000000; } ";
     Glib::RefPtr<Gtk::CssProvider> cssprov = Gtk::CssProvider::create();
     cssprov->load_from_data(style_sheet);
     style->add_provider(cssprov, GTK_STYLE_PROVIDER_PRIORITY_USER);
     style->add_class("color_bg");
     style->context_save();
}

//Display label color
void Gtk_Tuner::ColorDisplay(int IndexLabel,Gdk::RGBA Color, bool bColor)
{
     //For all the labels, no color
     for (int i=0;i<6;i++)
     {
	  Reset_Color_EventBox(Eventbox[i].get_style_context(), i);
	  Eventbox[i].queue_draw();
     }
     if (bColor) //if label must be have a color
     {
	  m_style=Eventbox[IndexLabel].get_style_context();
	  Change_Color_Widget(m_style,Color);
	  Eventbox[IndexLabel].queue_draw();
     }
}

//Update the precision slider
void Gtk_Tuner::UpdateSliderAccurate(double Delta)
{
     if (Delta>1.01)
     {
	  hsAccurate.set_value(100);
     }
     else if (Delta<0.99)
	  hsAccurate.set_value(0);
     else if ((Delta>1) && (Delta<1.01))
	  hsAccurate.set_value(hsAccurate.get_adjustment()->get_upper()/2+(Delta-1)*5000);
     else if ((Delta<1) && (Delta>0.99))
	  hsAccurate.set_value(hsAccurate.get_adjustment()->get_upper()/2-(1-Delta)*5000);
     //Green color if the note is perfect (no succeed)
     if (Delta>0.9995 && Delta<1.0005)
     {
	  m_style=Eventbox[6].get_style_context();
	  Change_Color_Widget(m_style,Gdk::RGBA("rgb(0,255,0)"));
     }
     else
     {
	  Reset_Color_EventBox(Eventbox[6].get_style_context(), 6);
     }
     Eventbox[6].queue_draw();
}

void Gtk_Tuner::Reset_Color_EventBox(Glib::RefPtr<Gtk::StyleContext> style, unsigned int index)
{
     std::vector<Glib::ustring> vec_list_class=style->list_classes();
     for(size_t j=0;j<vec_list_class.size();j++)
     {
	  style->remove_class(vec_list_class.at(j));
     }
}

//When click on Quit
void Gtk_Tuner::on_Quit()
{
     on_wndGuitarTuner_hide();
}

//When click on properties menu
void Gtk_Tuner::on_menuProperties()
{
     m_old_SoundCard=param_xml.szName_sound_card;
     m_old_PeriodCalibrate=param_xml.nCalibratePeriod;
     m_old_Sample=param_xml.nNumberSample;
     onproperties();
}

//Load parameters from the xml file
void Gtk_Tuner::Load_parameters(std::string szFileParam)
{
     bool ret;   //return the errors
     const char *value; //parameter value
     xmlDocPtr xmlparameters_doc = NULL; //pointer on the doc
     xmlXPathContextPtr xmlparameters_context = NULL;    //Pointer on the context
     xmlXPathObjectPtr xmlobject;    //Pointer on the object
     xmlNodePtr node;    //node
     ret=false;
     xmlparameters_doc = xmlParseFile (szFileParam.c_str());  //Get the doc  pointer 
     if (xmlparameters_doc)
     {
	  xmlXPathInit ();    //Path Initialization
	  xmlparameters_context = xmlXPathNewContext (xmlparameters_doc); //Get the context pointer
	  if (xmlparameters_context)
	  {
	       ret=true;
	       //Get the first object
	       xmlobject = xmlXPathEval (BAD_CAST "/PARAMETRES/HEADER/Visualisation_Spectrale/text()", xmlparameters_context);
	       if (xmlobject->nodesetval && xmlobject->nodesetval->nodeNr==1)
	       {
		    //the node
		    node = xmlobject->nodesetval->nodeTab[0];
		    //and this value
		    value=(const char*)node->content;
		    //Update the parameters
		    param_xml.bDispFreq=atoi(value);
	       }
	       //Same thing for all the parameters
	       xmlobject = xmlXPathEval (BAD_CAST "/PARAMETRES/HEADER/Frequence_Echantillonnage/text()", xmlparameters_context);
	       if (xmlobject->nodesetval && xmlobject->nodesetval->nodeNr==1)
	       {
		    node = xmlobject->nodesetval->nodeTab[0];
		    value=(const char*)node->content;
		    param_xml.nFreqSampling=atoi(value);
	       }
	       xmlobject = xmlXPathEval (BAD_CAST "/PARAMETRES/HEADER/Nombre_Echantillon/text()", xmlparameters_context);
	       if (xmlobject->nodesetval && xmlobject->nodesetval->nodeNr==1)
	       {
		    node = xmlobject->nodesetval->nodeTab[0];
		    value=(const char*)node->content;
		    param_xml.nNumberSample=atoi(value);
	       }
	       xmlobject = xmlXPathEval (BAD_CAST "/PARAMETRES/HEADER/Numero_Accord/text()", xmlparameters_context);
	       if (xmlobject->nodesetval && xmlobject->nodesetval->nodeNr==1)
	       {
		    node = xmlobject->nodesetval->nodeTab[0];
		    value=(const char*)node->content;
		    param_xml.nNumberChord=atoi(value);
	       }
	       xmlobject = xmlXPathEval (BAD_CAST "/PARAMETRES/HEADER/Numero_Carte_Son/text()", xmlparameters_context);
	       if (xmlobject->nodesetval && xmlobject->nodesetval->nodeNr==1)
	       {
		    node = xmlobject->nodesetval->nodeTab[0];
		    value=(const char*)node->content;
		    param_xml.nNumber_sound_card=atoi(value);
	       }
	       xmlobject = xmlXPathEval (BAD_CAST "/PARAMETRES/HEADER/Nom_Carte_Son/text()", xmlparameters_context);
	       if (xmlobject->nodesetval && xmlobject->nodesetval->nodeNr==1)
	       {
		    node = xmlobject->nodesetval->nodeTab[0];
		    value = (const char *)node->content;
		    param_xml.szName_sound_card = value;
	       }
	       xmlobject = xmlXPathEval (BAD_CAST "/PARAMETRES/HEADER/Valeur_Attenuation/text()", xmlparameters_context);
	       if (xmlobject->nodesetval && xmlobject->nodesetval->nodeNr==1)
	       {
		    node = xmlobject->nodesetval->nodeTab[0];
		    value=(const char*)node->content;
		    param_xml.nAttenuation=atoi(value);
	       }
	       xmlobject = xmlXPathEval (BAD_CAST "/PARAMETRES/HEADER/Interface_Graphique/text()", xmlparameters_context);
	       if (xmlobject->nodesetval && xmlobject->nodesetval->nodeNr==1)
	       {
		    node = xmlobject->nodesetval->nodeTab[0];
		    value=(const char*)node->content;
		    param_xml.nDisplay_Interface=atoi(value);
	       }
	       xmlobject = xmlXPathEval (BAD_CAST "/PARAMETRES/HEADER/Frequence_Repere/text()", xmlparameters_context);
	       if (xmlobject->nodesetval && xmlobject->nodesetval->nodeNr==1)
	       {
		    node = xmlobject->nodesetval->nodeTab[0];
		    value=(const char*)node->content;
		    param_xml.bDispMark=atoi(value);
	       }
	       xmlobject = xmlXPathEval (BAD_CAST "/PARAMETRES/HEADER/Note/text()", xmlparameters_context);
	       if (xmlobject->nodesetval && xmlobject->nodesetval->nodeNr==1)
	       {
		    node = xmlobject->nodesetval->nodeTab[0];
		    value=(const char*)node->content;
		    param_xml.szNote=value;
	       }
	       xmlobject = xmlXPathEval (BAD_CAST "/PARAMETRES/HEADER/Octave/text()", xmlparameters_context);
	       if (xmlobject->nodesetval && xmlobject->nodesetval->nodeNr==1)
	       {
		    node = xmlobject->nodesetval->nodeTab[0];
		    value=(const char*)node->content;
		    param_xml.sOctave=atoi(value);
	       }
	       xmlobject = xmlXPathEval (BAD_CAST "/PARAMETRES/HEADER/Frequence/text()", xmlparameters_context);
	       if (xmlobject->nodesetval && xmlobject->nodesetval->nodeNr==1)
	       {
		    node = xmlobject->nodesetval->nodeTab[0];
		    value=(const char*)node->content;
		    param_xml.dFrequency=atof(value);
	       }
	       xmlobject = xmlXPathEval (BAD_CAST "/PARAMETRES/HEADER/Mode_Accordeur/text()", xmlparameters_context);
	       if (xmlobject->nodesetval && xmlobject->nodesetval->nodeNr==1)
	       {
		    node = xmlobject->nodesetval->nodeTab[0];
		    value=(const char*)node->content;
		    param_xml.sTunerMode=atoi(value);
	       }
	       xmlobject = xmlXPathEval (BAD_CAST "/PARAMETRES/HEADER/IndexNote/text()", xmlparameters_context);
	       if (xmlobject->nodesetval && xmlobject->nodesetval->nodeNr==1)
	       {
		    node = xmlobject->nodesetval->nodeTab[0];
		    value=(const char*)node->content;
		    param_xml.sIndexNote=atoi(value);
	       }
	       xmlobject = xmlXPathEval (BAD_CAST "/PARAMETRES/HEADER/Periode_Calibration/text()", xmlparameters_context);
	       if (xmlobject->nodesetval && xmlobject->nodesetval->nodeNr==1)
	       {
		    node = xmlobject->nodesetval->nodeTab[0];
		    value=(const char*)node->content;
		    param_xml.nCalibratePeriod=atoi(value);
	       }
	       xmlobject = xmlXPathEval (BAD_CAST "/PARAMETRES/HEADER/Persistence/text()", xmlparameters_context);
	       if (xmlobject->nodesetval && xmlobject->nodesetval->nodeNr==1)
	       {
		    node = xmlobject->nodesetval->nodeTab[0];
		    value=(const char*)node->content;
		    param_xml.Persistence_Note_Delay=atoi(value);
	       }
	       xmlXPathFreeObject(xmlobject);
	       xmlXPathFreeContext(xmlparameters_context);
	  }
	  else
	  {
	       std::string message(_("Problem to create a xml context"));
	       Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
	       Dialog.set_title(_("XML Problem"));
	       Dialog.run();
	  }
	  xmlFreeDoc(xmlparameters_doc); //memory free
     }
     if (!ret)   //If parameters load problem
     {
	  if (param_xml.bFileOK)   //And If the file exist
	  {
	       //Keep the old parameters
	       std::string message(_("Cannot read the parameters\n"));
	       message+=_("Old parameters are kept");
	       Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
	       Dialog.set_title(_("Unknown parameters"));
	       Dialog.run();
	  }
	  else
	  {
	       //If no file, no analysis
	       std::string message(_("Cannot read the parameters\n"));
	       message+=_("The analyse is impossible");
	       Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
	       Dialog.set_title(_("Unknown parameters"));
	       Dialog.run();
	       //Menu not activate
	       m_tbanalyse->set_sensitive(false);
	  }
     }
}

//Update the path of the Chord xml file
void Gtk_Tuner::SetPathChordXML(std::string szDirectory)
{
     m_szdirectory_chord_xml=szDirectory;
}

//Properties loading
void Gtk_Tuner::Load_Properties(void)
{
     xmlTextReaderPtr reader;    //Pointer on the properties
     Gtk::Dialog *dialog;
     reader=xmlReaderForFile(m_szparameters_xml.c_str(),NULL,0); //Get this pointer
     if (m_dlgProperties)
     {
	  int ret;
	  ret=m_dlgProperties->Load_Sound_Card_List();   //Load all the sound card
	  if (ret==-1)
	       bSoundCard=false;
	  else
	       bSoundCard=true;
	  ret=m_dlgProperties->Chords_Load(m_szdirectory_chord_xml);  //Load the chords
	  if (ret==-1)
	       bChord=false;
	  else
	       bChord=true;
     }
     if (reader!=NULL)
     {
	  UpdateControls(); //If the parameters file exist, update the controls
     }
     else
     {
	  param_xml.bFileOK=false;
#ifdef __linux__
	  struct stat st;
	  if(stat(m_szHome.c_str(),&st) != 0) //If no directory .guitar_tuner
	  {
	       if (mkdir(m_szHome.c_str(),0777) !=0)   //Create this directory
	       {
		    std::string message(_("Cannot create the .guitar_tuner directory\n"));
		    message+=_("The software stopped");
		    Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
		    Dialog.set_title(_("Problem to create directory"));
		    Dialog.run();
		    on_wndGuitarTuner_hide();
	       }
	  }
#endif
	  onproperties(); //OPen the properties dialog box
     }
     xmlFreeTextReader(reader);  //Free the memory
}

//Update the controls
void Gtk_Tuner::UpdateControls(void)
{
     char *carac;
     std::string temp;
     std::ostringstream stream_o;
     int err_card, err_chord;
     if ((param_xml.nDisplay_Interface==0) && (Vbox1.get_children().size()!=2))
	  Vbox1.remove(Vbox3);
     else if (((param_xml.nDisplay_Interface==1) || (param_xml.nDisplay_Interface==2)) && (Vbox1.get_children().size()!=2))
     {
	  if (m_Display!=NULL)
	  {
	       Vbox1.remove(*m_Display);
	  }
     }
     //Load the properties with the file
     Load_parameters(m_szparameters_xml);
     if ((err_card=SoundCard_changed())<0)
     {
	  //No analysis if problem with the sound card
	  m_old_SoundCard="";
	  m_tbanalyse->set_sensitive(false);
	  std::string message(_("The default sound card can't get loaded\n"));
	  message+=_("Try to load an other sound card");
	  Gtk::MessageDialog Dialog(message.c_str(),false,Gtk::MESSAGE_ERROR,Gtk::BUTTONS_CLOSE,false );
	  Dialog.set_title(_("Problem to load the default sound card"));
	  Dialog.run();
     }
     else
     {
	  //Get the sound card name
	  temp=soundcard.GetNameSndCard(param_xml.nNumber_sound_card);
	  Glib::ustring card=Glib::ustring::compose(_("Sound card: %1"),temp);
	  stsInfo[0].push(card,ctx[0]); //Update the status bars
	  stream_o<<param_xml.nFreqSampling;
	  Glib::ustring sample=Glib::ustring::compose(_("Sample: %1"),stream_o.str());  
    
	  stsInfo[2].push(sample,ctx[2]); 
	  stream_o.str("");
	  stream_o<<param_xml.nNumberSample;
	  Glib::ustring samples=Glib::ustring::compose(_("Samples: %1"),stream_o.str());
	  stsInfo[3].push(samples,ctx[3]);
     }
     param_xml.bFileOK=true;
     if ((err_chord=Chord_changed())<0)
     {
	  //Same thing with the chord file
	  m_tbanalyse->set_sensitive(false);
     }
     else
     {
	  Glib::ustring chord=Glib::ustring::compose(_("Chord: %1"),Notes_Chord.GetNameChord());  
	  stsInfo[1].push(chord,ctx[1]); 	 
     }
     //If all is OK, calibrate the sound card
     if ((err_card==0) && (err_chord==1) && ((soundcard.GetNameSndCard(param_xml.nNumber_sound_card)!=m_old_SoundCard) || (param_xml.nCalibratePeriod!=m_old_PeriodCalibrate) || (param_xml.nNumberSample!=m_old_Sample)))
     {
	  m_dlgProperties->SetPathParam_XML(m_szparameters_xml);    //and file directory
	  m_dlgProperties->SetParameters(param_xml);
	  onbuttoncalibrate();
     }
//Update the display selected
     switch(param_xml.nDisplay_Interface)
     {
     case 0:
	  Display_Standard();
	  break;
     case 1:
	  Display_Round();
	  break;
     case 2:
	  Display_Tuner();
	  break;
     default:
	  Display_Tuner();
     }
}

//Display the dialog box About
void Gtk_Tuner::on_menuAbout()
{
     m_refGlade->get_widget("aboutdialog1",m_dlgAbout);  //Get the dialog box pointer
     m_dlgAbout->run();
     m_dlgAbout->hide();
}
