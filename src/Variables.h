#include <gtkmm.h>
#ifndef VARIABLES_H_INCLUDED
#define VARIABLES_H_INCLUDED
#include <libintl.h>
#define _(String) gettext (String)
const std::string Name_Note[17]=
{
     _("C"),
     _("C#"),
     _("Db"),
     _("D"),
     _("D#"),
     _("Eb"),
     _("E"),
     _("F"),
     _("F#"),
     _("Gb"),
     _("G"),
     _("G#"),
     _("Ab"),
     _("A"),
     _("A#"),
     _("Bb"),
     _("B"),
};
// Structure for the parameters saved
typedef struct parameters
{
    unsigned int nNumber_sound_card;  //index number of the sound card
     std::string szName_sound_card; //The na me of the sound card
    unsigned int nFreqSampling;   //The sampling frequency of the sound card
    unsigned int nNumberSample;   //Sample quantity used to make the FFT
     unsigned int nCalibratePeriod; //Period to calibrate the sound card
     unsigned int period;//Channel quantity of the sound card
    unsigned int nNumberChord;  //Index number of the chord
    bool bDispFreq;  //Boolean for displaying the spectrum window
    unsigned int nAttenuation; //Signal Attenuation
     unsigned int Persistence_Note_Delay; //Persistence of the note
    bool bFileOK;    //Boolean to know if the xml file exist
     int nDisplay_Interface;//Interface index (1 : Standard, 2 : Round, 3 : Tuner)
     bool bDispMark;//Boolean for displaying the note mark
     std::string  szNote; //Name of the note in the combo
     unsigned short sIndexNote; //Index of the note in the combo
     unsigned short sOctave; //Octave
     double dFrequency; //Frequency to chord
     unsigned short sTunerMode; //The working mode of the tuner
}_parameters;
//Structure for displaying notes (color, frquency, found, etc..)
typedef struct _display_parameters
{
     bool bcolor; //Boolean to know if a color must be frame a note
     int Index_Note_Below, Index_Note_Sup, Index_Note_Near; //Indexes for locate the frequency peak
     double Note_Below, Note_Sup, Frequency_Peak,precision_ratio,Level_Max; //The same but with the frequencies
     Gdk::RGBA Color; //The color of the note frame
} display_parameters;
//Structures for calculating the FFT
typedef struct
{
     double *RealOut;
     double *ImagOut;
}out;

typedef struct
{
     unsigned int num_Samples;
     unsigned int period;
     out data_out;
     double *RealIn;
     double *ImagIn;
     bool m_calibration;
}s_data;
class ModelColumns : public Gtk::TreeModel::ColumnRecord    //Class for the combos
    {
    public:
        ModelColumns()  //Constructor
        {
            add(m_col_id);  //Add the line number
            add(m_col_name);    //Add the line name
        }
        Gtk::TreeModelColumn<int> m_col_id; // Index of the line
        Gtk::TreeModelColumn<std::string> m_col_name; //Column name
    };
#endif // VARIABLES_H_INCLUDED
